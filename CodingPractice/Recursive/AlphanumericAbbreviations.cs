﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpLab.RecursiveProblems
{
    // http://www.geeksforgeeks.org/alphanumeric-abbreviations-of-a-string/   
    // also check http://www.geeksforgeeks.org/alphanumeric-abbreviations-of-a-string/ 
    public class AlphanumericAbbreviations
    {
        public void Alg(string input)
        {
            AllCombination(input, "", 0, input.Length);
            //var res = from i in input
            //          from d in input                      
            //          select new { i, d};

            //foreach (var item in res)
            //{
            //    Console.WriteLine(item.i + "" +item.d);
            //}        
        }
        private void Combination(string input, string res, int s, int len)
        {
            if (len <= 0) { 
                Console.WriteLine(res);
                return;
            }

            for (int i = s; i <= input.Length - len; i++)
            {
                Combination(input, res + input[i], s + 1, len -1);
            }
        }
        // http://www.geeksforgeeks.org/alphanumeric-abbreviations-of-a-string/
        private void AllCombination(string input, string res, int s, int len)
        {
            if (s == len)
            {
                Console.WriteLine(res);
                return;
            }
            res = res + input[s];
            AllCombination(input, res, s + 1, len);
            res = res.Substring(0, res.Length - 1);
            AllCombination(input, res, s + 1, len);
                                   
        }

        public static void Main(string[] args)
        {
            Console.WriteLine("********************Start********************");

            var alg = new AlphanumericAbbreviations();           
            string str = "ABC";

            Console.WriteLine("Input: " + string.Join(", ", str));                                   
            alg.Alg(str);                       
            Console.WriteLine("*********************End*********************");
        }
    }
}
