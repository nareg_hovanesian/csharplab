﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpLab.Recursive
{
    public class PrintBalanceParenthesesz
    {
        public void Alg(int num)
        {
            Alg(num, num, "");
        }

        // num = 3 
        //  ((()))
        //  (()())
        //  (())()
        //  ()(())
        //  ()()()
        // num = 4
        //  (((())))
        //  ((()()))
        //  ((())())
        //  ((()))()
        //  (()(()))
        //  (()()())
        //  (()())()
        //  (())(())
        //  (())()()
        //  ()((()))
        //  ()(()())
        //  ()(())()
        //  ()()(())
        //  ()()()()
        private void Alg(int L, int R, string str)
        {
            if (R < L || L < 0)
                return;
            if (L == 0 && R == 0) { 
                Console.WriteLine(str);
                return;
            }
            if (L > 0)
                Alg(L - 1, R, str + "(");
            if(R > L)
                Alg(L, R - 1, str + ")");
        }
        static void Main(string[] args)
        {
            Console.WriteLine("********************Start********************");

            new PrintBalanceParenthesesz().Alg(3);

            Console.WriteLine("*********************End*********************");
        }
    }
}
