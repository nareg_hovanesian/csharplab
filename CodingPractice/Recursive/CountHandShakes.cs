﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpLab.Recursive
{
    public class CountHandShakes
    {
        // 2  3  4   5   6   7....
        // 1, 3, 6, 10, 15, 21,....
        private int Alg(int num)
        {            
            if (num < 2)
                return 0;
            if (num == 2)
                return 1;
            return Alg(num - 1) + num - 1;
        }
      
        static void Main(string[] args)
        {
            Console.WriteLine("********************Start********************");
            
            Console.WriteLine("CountHandShakes: " + new CountHandShakes().Alg(3));

            Console.WriteLine("*********************End*********************");
        }
    }
}
