﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpLab.Recursive
{
    // https://codefights.com/task/mGdjBK8AfJFf9TDMy
    // Math stuff 
    // You have n white balls, m blue balls and k black balls placed next to each other in one row.
    // Find the number of unique permutations of all the balls with no two adjacent balls being both blue modulo 10^9 + 7.
    public class BallPermutationProblem1
    {
        public static int ballPermutations(int N, int M, int K)
        {
            // 2 4 2
            int NK = N + K;
            int S = (int)Math.Pow(10, 9) + 7;
            //int res1 = (int) ((Perm(NK) % S) / ((Perm(N) % S) * (Perm(K) % S)));
            long res1 = Comb(NK, N) % S;
            //int res2 = (int)(Perm(NK + 1) / (Perm(M) * Perm(NK + 1 - M)));
            long res2 = (Comb(NK + 1, M) % S);
            return (int)((res1 * res2) % S);
        }

        public static long Comb(long n, long m)
        {
            int S = (int)Math.Pow(10, 9) + 7;
            long k = n - m;
            long p = k > m ? k : m;
            long res = 1;
            List<long> dividents = new List<long>();
            List<long> divisers = new List<long>();

            for (long i = n; i > p; i--)
            {
                dividents.Add(i);
            }

            for (long i = n - p; i > 1; i--)
            {
                divisers.Add(i);
            }
            for (int i = 0; i < divisers.Count; i++)
            {
                for (int j = 0; j < dividents.Count; j++)
                {
                    long d = GCD(divisers[i], dividents[j]);
                    divisers[i] /= d;
                    dividents[j] /= d;
                }
            }

            foreach (var item in dividents)
            {
                res *= item;
                res %= S;
            }
            foreach (var item in divisers)
            {
                res /= item;
            }
            return res % S;
        }
        public static long GCD(long[] numbers)
        {
            return numbers.Aggregate(GCD);
        }

        public static long GCD(long a, long b)
        {
            //Console.WriteLine(a + " " + b);
            return b == 0 ? a : GCD(b, a % b);
        }
        public static void Main(string[] args)
        {
            Console.WriteLine("********************Start********************");



            Console.WriteLine("*********************End*********************");
        }
    }
}
