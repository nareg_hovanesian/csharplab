﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpLab.Graph.Trees.BinaryTree
{
    public class LargestValuesInTreeRows
    {
        public class Tree<T>
        {
            public T value { get; set; }
            public Tree<T> left { get; set; }
            public Tree<T> right { get; set; }
        }

        // You have a binary tree t.Your task is to find the largest value in each row of this tree.In a tree, a row is a set of nodes that have equal depth. For example, a row with depth 0 is a tree root, a row with depth 1 is composed of the root's children, etc.
        // Return an array in which the first element is the largest value in the row with depth 0, the second element is the largest value in the row with depth 1, the third element is the largest element in the row with depth 2, etc.
        public static int[] largestValuesInTreeRows(Tree<int> t)
        {
            List<int> list = new List<int>();
            if (t == null)
                return list.ToArray();
            largestValuesInTreeRows(t, 0, list);
            return list.ToArray();    
        }
        public static void largestValuesInTreeRows(Tree<int> t, int hight, List<int> list)
        {
            if (t == null)
                return;
            if (list.Count <= hight)
                list.Add(t.value);
            else if (list[hight] < t.value)
            {
                list[hight] = t.value;
            }
            largestValuesInTreeRows(t.left, hight + 1, list);
            largestValuesInTreeRows(t.right, hight + 1, list);
        }

        // https://codefights.com/interview-practice/task/m9vC4ALaAeudkwRTF
        public static void Main(string[] args)
        {
            Console.WriteLine("********************Start********************");



            Console.WriteLine("*********************End*********************");
        }
    }
}
