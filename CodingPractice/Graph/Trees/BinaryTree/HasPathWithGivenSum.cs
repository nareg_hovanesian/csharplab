﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpLab.Graph.Trees.BinaryTree
{
 
    public class HasPathWithGivenSum
    {
        public class Tree<T>
        {
            public T value { get; set; }
            public Tree<T> left { get; set; }
            public Tree<T> right { get; set; }
        }

        // Given a binary tree t and an integer s, determine whether there is a root to leaf path in t such that the sum of vertex values equals s.
        // Correct 
        // https://codefights.com/interview-practice/task/TG4tEMPnAc3PnzRCs/description
        public bool funcHasPathWithGivenSum(Tree<int> t, int s)
        {
            if (t == null && s == 0)
                return true;
            if (t == null)
                return false;
            Stack<Tree<int>> stack = new Stack<Tree<int>>();
            Stack<int> sums = new Stack<int>();
            stack.Push(t);
            int sum = 0;
            while (stack.Count > 0)
            {
                var t1 = stack.Pop();
                sum += t1.value;

                if ((t1.left == null && t1.right == null) && sum == s)                    
                    return true;
                if (t1.left == null && t1.right == null && sums.Count > 0)
                    sum = sums.Pop();

                if (t1.left != null) stack.Push(t1.left);
                if (t1.right != null) stack.Push(t1.right);
                if (t1.left != null && t1.right != null)
                    sums.Push(sum);
            }
            return false;
        }
        public static void Main(string[] args)
        {
            Console.WriteLine("********************Start********************");



            Console.WriteLine("*********************End*********************");
        }

    }
}
