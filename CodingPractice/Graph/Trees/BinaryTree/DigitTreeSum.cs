﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpLab.Graph.Trees.BinaryTree
{
    public class DigitTreeSum
    {
        public class Tree<T>
        {
            public T value { get; set; }
            public Tree<T> left { get; set; }
            public Tree<T> right { get; set; }
        }
        public static long digitTreeSum(Tree<int> t)
        {            
            List<string> list = new List<string>();
            digits(t, "", list);
            long total = 0;
            foreach (var item in list)
            {
                if(!string.IsNullOrWhiteSpace(item))                
                    total += long.Parse(item);
            }
            return total;
        }

        public static void digits(Tree<int> t, string str, List<string> list)
        {
            if (t == null)
                return;
            if (t.left == null && t.right == null)
            {
                list.Add(str + t.value.ToString());
                return;
            }
            if (t.left != null)
                digits(t.left, str + t.value.ToString(), list);
            digits(t.right, str + t.value.ToString(), list);
        }



        // https://codefights.com/interview-practice/task/2oxNWXTS8eWBzvnRB
        public static void Main(string[] args)
        {
            Console.WriteLine("********************Start********************");



            Console.WriteLine("*********************End*********************");
        }

    }
}
