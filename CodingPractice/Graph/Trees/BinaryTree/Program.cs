﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpLab.Graph.Trees.BinaryTree
{    
    public class Program
    {       
        public class Node<T> where T : IComparable {
            public T Data { get; set; }
            public Node<T> Left { get; set; }
            public Node<T> Right { get; set; }
            public Node(T var = default(T))
            {
                Data = var;
            }
        }
        public class BinaryTree<T> where T : IComparable
        {
            public Node<T> Root { get; set; }
            public BinaryTree() { }                     
            
            private void InorderTraversal(Node<T> node)
            {
                if (node == null)
                    return;                
                InorderTraversal(node.Left);
                Console.Write(node.Data);
                InorderTraversal(node.Right);
            }

            private void PostOrderTraversal(Node<T> node)
            {
                if (node == null)
                    return;
                PostOrderTraversal(node.Left);
                PostOrderTraversal(node.Right);
                Console.Write(node.Data);
            }

            private void PreOrderTraversal(Node<T> node)
            {
                if (node == null)
                    return;
                Console.Write(node.Data);
                PreOrderTraversal(node.Left);
                PreOrderTraversal(node.Right);
            }

            public Node<T> LCA(Node<T> n1, Node<T> n2)
            {
                return LCA(Root, n1, n2);
            }

            // Lowest Common Ancestor 
            // things to consider: if one node is ancestor of the other node, it will return that node.
            private Node<T> LCA(Node<T> root, Node<T> n1, Node<T> n2) 
            {
                if (root == null)
                    return null;
                if(root.Data.CompareTo(n1.Data) == 0 || root.Data.CompareTo(n2.Data) == 0)               
                    return root;
                                
                var Left = LCA(root.Left, n1, n2);
                var Right = LCA(root.Right, n1, n2);

                if (Left != null && Right != null)
                    return root;
                if (Left == null && Right == null)
                    return null;                

                return Left == null ?  Right : Left;
            }

            public void InorderTraversal()
            {
                InorderTraversal(Root);
            }
            public void PreOrderTraversal()
            {
                PreOrderTraversal(Root);
            }
            public void PostOrderTraversal()
            {
                PostOrderTraversal(Root);
            }

        }
        public class RandomTree
        {
            Random rand = new Random();
            private Node<int> GenerateRandomNode(Node<int> node)
            {
                if (node == null)
                    return GenerateRandomNode(new Node<int>());                
                node.Left  = rand.Next() % 2 == 0 ? GenerateRandomNode(node.Left) : null;                
                node.Right = rand.Next() % 2 == 0 ? GenerateRandomNode(node.Right) : null;
                node.Data = rand.Next();
                return node;
            }
            public BinaryTree<int> GenerateRandomTree()
            {
                BinaryTree<int> tree = new BinaryTree<int>();
                tree.Root = new Node<int>();
                GenerateRandomNode(tree.Root);
                return tree;
            }
        }

        // https://www.youtube.com/watch?v=r2Vn6ztdSP0
        private int LongestConsecutiveBranch(Node<int> node)
        {
            if (node == null) return 0;
            return LongestConsecutiveBranch(node, node.Data, 0);
        }
        private int LongestConsecutiveBranch(Node<int> node, int preVal, int length)
        {
            if (node == null)
            {
                return length;
            }
            if (preVal + 1 == node.Data)
            {
                int leftMax = LongestConsecutiveBranch(node.Left, node.Data, length + 1);
                int rightMax = LongestConsecutiveBranch(node.Right, node.Data, length + 1);
                return Max(leftMax, rightMax);
            } else
            {
                int leftMax = LongestConsecutiveBranch(node.Left, node.Data, 1);
                int rightMax = LongestConsecutiveBranch(node.Right, node.Data, 1);
                return Max(leftMax, rightMax, length);
            }          
        }
        private int Max(params int[] nums)
        {
            return nums.Max();
        }
        public int Sum(Node<int> node)
        {
            if (node == null) return 0;
            return Sum(node.Left) + Sum(node.Right);
        }
        public bool IsBinarySearchTree(Node<int> node)
        {
            if(node.Left != null)
            {
                if (node.Data < node.Left.Data)
                    return false;
                IsBinarySearchTree(node.Left);
            }
            if (node.Right != null)
            {
                if (node.Data > node.Right.Data) 
                    return false;
                IsBinarySearchTree(node.Right);
            }
            return true;
        }
        public static void Main(string[] args)
        {
            Console.WriteLine("********************Start********************");

            //RandomTree rand = new RandomTree();
            //var RandTree = rand.GenerateRandomTree();
            //RandTree.InorderTraversal();
            var tree = new BinaryTree<int>();

            tree.Root = new Node<int>(0);
            tree.Root.Left = new Node<int>(2);
            tree.Root.Right = new Node<int>(4);
            tree.Root.Left.Left = new Node<int>(3);
            tree.Root.Left.Left.Right = new Node<int>(6);
            tree.Root.Left.Right = new Node<int>(5);



            tree.InorderTraversal();
            Console.WriteLine();
            tree.PreOrderTraversal();
            Console.WriteLine();
            tree.PostOrderTraversal();
            Console.WriteLine();            
            Console.WriteLine("LongestConsecutiveBranch: " + new Program().LongestConsecutiveBranch(tree.Root));            
            Console.WriteLine("Lowest Common Ancestor:" + tree.LCA(tree.Root.Left.Left, tree.Root.Left.Right)?.Data);


            Console.WriteLine("*********************End*********************");
        }
    }
}
