﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpLab.Graph.Trees.BinaryTree
{
    public class IsSubTree
    {
        public class Tree<T>
        {
            public T value { get; set; }
            public Tree<T> left { get; set; }
            public Tree<T> right { get; set; }
        }
        public static bool isSubtree(Tree<int> t1, Tree<int> t2)
        {
            if (t2 == null) return true;
            if (t1 == null) return false;
            if (t1.value == t2.value && equals(t1.left, t2.left) && equals(t1.right, t2.right))
                return isSubtree(t1.left, t2.left) && isSubtree(t1.right, t2.right);
            return isSubtree(t1.left, t2) || isSubtree(t1.right, t2);
        }

        public static bool equals(Tree<int> t1, Tree<int> t2)
        {
            if (t1 == null && t2 == null)
                return true;
            if ((t1 != null && t2 == null) || (t1 == null && t2 != null))
                return false;
            if (t1.left != null && t2.left != null && t1.left.value != t2.left.value)
                return false;
            if (t1.right != null && t2.right != null && t1.right.value != t2.right.value)
                return false;
            return true;
        }

        public static void Main(string[] args)
        {
            https://codefights.com/interview-practice/task/mDpAJnDQkJqaYYsCg/solutions
            Console.WriteLine("********************Start********************");

            Console.WriteLine("*********************End*********************");
        }
    }
}



//{"value":1,"left":{"value":2,"left":null,"right":null},"right":{"value":3,"left":null,"right":null}}


//{"value":1,"left":{"value":2,"left":null,"right":null},"right":{"value":3,"left":null,"right":{"value":1,"left":{"value":2,"left":null,"right":null},"right":{"value":3,"left":null,"right":null}}}}


//{"value":1,"left":{"value":1,"left":{"value":2,"left":null,"right":null},"right":{"value":3,"left":null,"right":null}},"right":{"value":3,"left":null,"right":null}}


//{"value":1,"left":{"value":2,"left":{"value":1,"left":{"value":2,"left":null,"right":null},"right":null},"right":null},"right":null}

//{"value":1,"left":{"value":1,"left":null,"right":null},"right":{"value":1,"left":{"value":2,"left":null,"right":null},"right":null}}


//{"value":2,"left":{"value":1,"left":null,"right":null},"right":{"value":2,"left":{"value":2,"left":{"value":1,"left":{"value":2,"left":null,"right":null},"right":null},"right":null},"right":null}}