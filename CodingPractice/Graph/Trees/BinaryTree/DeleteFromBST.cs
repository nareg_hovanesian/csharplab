﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpLab.Graph.Trees.BinaryTree
{
    public class DeleteFromBST
    {

        public class Tree<T>
        {
            public T value { get; set; }
            public Tree<T> left { get; set; }
            public Tree<T> right { get; set; }
        }
        public static Tree<int> deleteFromBST(Tree<int> t, int[] queries)
        {            
            foreach (var item in queries)
            {
                delete(t, item);
            }
            return t;
        }
        public static Tree<int> delete(Tree<int> t, int val)
        {
            if (t == null)
                return null;
            if (t.value == val)
            {
                if (t.left == null)
                    return t = t.right;
                else if (t.right == null)
                    return t.left;

                var v = t.left;                
                while (v.right != null)                
                    v = v.right;
                t.value = v.value;
                t.left = delete(t.left, t.value);                
                return t;
            }

            if (t.value < val)
            {
                t.right = delete(t.right, val);
                return t;
            }
            t.left = delete(t.left, val);
            return t;                       
        }
        public static void Main(string[] args)
        {
            // https://codefights.com/interview-practice/task/oZXs4td52fsdWC9kR
            Console.WriteLine("********************Start********************");
            deleteFromBST(new Tree<int>(), null);

            Console.WriteLine("*********************End*********************");
        }
    }
}
