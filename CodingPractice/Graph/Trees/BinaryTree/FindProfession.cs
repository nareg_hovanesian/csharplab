﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpLab.Graph.Trees.BinaryTree
{
    public class FindProfession
    {
        // https://codefights.com/interview-practice/task/FwAR7koSB3uYYsqDp
        // Given the level and position of a person in the ancestor tree above, find the profession of the person.
        // Note: in this tree first child is considered as left child, second - as right.
        //         E
        //       /         \
        //      E D
        //    /   \        /  \
        //   E D      D E
        //  / \   / \    / \   / \
        // E D D E  D E E D
        public class Tree<T>
        {
            public T value { get; set; }
            public Tree<T> left { get; set; }
            public Tree<T> right { get; set; }
        }

        public static string findProfession(int level, int pos)
        {
            return findProfessionHelper(level, pos, false);
        }
        public static string findProfessionHelper(int level, int pos, bool reverse)
        {
            if (level < 1 || pos < 1)
                return "";
            if (pos == 1)
                return reverse ? "Doctor" : "Engineer";
            if (pos == 2)
                return reverse ? "Engineer" : "Doctor";
            int prevLevelWidth = 1 << (level - 2);
            if (pos <= prevLevelWidth)
                return findProfessionHelper(level - 1, pos, reverse);
            else
                return findProfessionHelper(level - 1, pos - prevLevelWidth, !reverse);
        }
        public static void Main(string[] args)
        {
            Console.WriteLine("********************Start********************");          
            findProfession(3, 3);
            Console.WriteLine("*********************End*********************");
        }
    }
}
