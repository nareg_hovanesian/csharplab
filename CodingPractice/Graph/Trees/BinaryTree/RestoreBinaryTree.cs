﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpLab.Graph.Trees.BinaryTree
{
    public class RestoreBinaryTree
    {
        public class Tree<T>
        {
            public T value { get; set; }
            public Tree<T> left { get; set; }
            public Tree<T> right { get; set; }
        }
        static int preIndex = 0; 
        public static Tree<int> restoreBinaryTree(int[] inorder, int[] preorder)
        {
            preIndex = 0;
            return restoreBinaryTree(inorder, preorder, 0, preorder.Count() -1);
        }

        static Tree<int> restoreBinaryTree(int[] inorder, int[] preorder, int L, int R)
        {
            if (L > R)
                return null;
            Tree<int> node = new Tree<int>() { value = preorder[preIndex++] };
            if (L == R)
                return node;
            int inIndex = Array.FindIndex(inorder, L, R-L + 1, i => i == node.value);
            node.left = restoreBinaryTree(inorder, preorder, L, inIndex - 1);
            node.right = restoreBinaryTree(inorder, preorder, inIndex + 1, R);
            return node;
        }
          
        public static void Main(string[] args)
        {
            https://codefights.com/interview-practice/task/AaWaYxi8gjtbqgp2M
            Console.WriteLine("********************Start********************");

            Console.WriteLine("*********************End*********************");
        }
    }
}
