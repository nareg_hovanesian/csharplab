﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpLab.Graph.Trees.BinaryTree
{
    public class TraverseTree
    {
        public class Tree<T>
        {
            public T value { get; set; }
            public Tree<T> left { get; set; }
            public Tree<T> right { get; set; }
        }

        public static int[] traverseTree(Tree<int> t)
        {
            List<int> list = new List<int>();
            if (t == null)
                return list.ToArray();
            Queue<Tree<int>> queue = new Queue<Tree<int>>();
            queue.Enqueue(t);

            while (queue.Count > 0)
            {
                var d = queue.Dequeue();
                list.Add(d.value);
                if (d.left != null)
                    queue.Enqueue(d.left);
                if (d.right != null)
                    queue.Enqueue(d.right);
            }
            return list.ToArray();
        }

        // https://codefights.com/interview-practice/task/PhNPP45hZGNwpPchi
        public static void Main(string[] args)
        {
            Console.WriteLine("********************Start********************");



            Console.WriteLine("*********************End*********************");
        }

    }
}
