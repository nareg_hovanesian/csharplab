﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpLab.Graph.Trees.BinaryTree
{
    public class IsTreeSymmetric
    {
        public class Tree<T>
        {
            public T value { get; set; }
            public Tree<T> left { get; set; }
            public Tree<T> right { get; set; }
        }

        // Given a binary tree t and an integer s, determine whether there is a root to leaf path in t such that the sum of vertex values equals s.
        // Correct 
        // https://codefights.com/interview-practice/task/TG4tEMPnAc3PnzRCs/description
        public static bool FuncIsTreeSymmetric(Tree<int> t)
        {
            List<int> list = new List<int>();                         
            InOrderTraversal(t, list);
            
            Console.WriteLine(IsAnagram(list));
            foreach (var item in list)
            {                
                Console.Write(item);
            }
            Console.WriteLine();
            return false;
        }

        public static string InOrderTraversal(Tree<int> t)
        {
            if (t == null)
                return "";           
            return InOrderTraversal(t.left) +  t.value + InOrderTraversal(t.right);
        }

        public static int? InOrderTraversal(Tree<int> t, List<int> list)
        {
            if (t == null)
                return null;

            var left = InOrderTraversal(t.left, list);
            if(left != null)
                list.Add((int)left );            
            list.Add(t.value);
            var right = InOrderTraversal(t.right, list);
            if (right != null)
                list.Add((int)right);
            return null;
            //return InOrderTraversal() +  t.value + InOrderTraversal(t.right);
        }

        public static bool IsAnagram(List<int> list)
        {
            if (list.Count == 1)
                return true;
            if (list.Count % 2 == 0)
                return false;
            int L = 0;
            int R = list.Count -1;
            while (L < R)
            {                
                if (list[L++] != list[R--])                
                    return false;                             
            }
            return true;
        }
        public bool IsAnagram(string str)
        {
            int i = 0;
            int j = str.Length - 1;

            while (i <= j)
            {                
                if (str[i++] != str[j--])
                    return false;
            }
            return true;
        }
        public static void Main(string[] args)
        {
            Console.WriteLine("********************Start********************");

            Tree<int> tree = new Tree<int>();
            tree.value = 1;
            tree.left = new Tree<int>();
            tree.left.value = 2;            
            tree.left.left = new Tree<int>();
            tree.left.left.value = 3;
            tree.left.right = new Tree<int>();
            tree.left.right.value = 4;

            tree.right = new Tree<int>();
            tree.right.value = 2;
            tree.right.left = new Tree<int>();
            tree.right.left.value = 4;
            tree.right.right = new Tree<int>();
            tree.right.right.value = 3;
            FuncIsTreeSymmetric(tree);


            Console.WriteLine("*********************End*********************");
        }
    }
}
