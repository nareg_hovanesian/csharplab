﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpLab.Graph.Trees.BinaryTree
{
    class KthSmallestInBST
    {
        public class Tree<T>
        {
            public T value { get; set; }
            public Tree<T> left { get; set; }
            public Tree<T> right { get; set; }
        }
        public static int KthSmallestInBSTalg(Tree<int> t, int k)
        {
            var first = t;
            Stack<Tree<int>> stack = new Stack<Tree<int>>();
           
            while (first != null) {
                stack.Push(first);
                first = first.left;
            }
            
            while (stack.Count > 0)
            {
                first = stack.Pop();
                if ( --k <= 0)
                {
                    break;
                }
                if (first.right != null)
                {
                    first = first.right;                
                    while (first != null)
                    {
                        stack.Push(first);
                        first = first.left;
                    }
                }
            }
            return first.value;            
        }     

        public static void Main(string[] args)
        {
            // https://codefights.com/interview-practice/task/jAKLMWLu8ynBhYsv6
            Console.WriteLine("********************Start********************");
            
            Console.WriteLine("*********************End*********************");
        }
    }
}
