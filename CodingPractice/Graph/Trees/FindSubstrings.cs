﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpLab.Graph.Trees
{
    public class FindSubstrings
    {

        public class TrieTree
        {
            public char Value;
            public Dictionary<char, TrieTree> Edges = new Dictionary<char, TrieTree>();
            public bool IsWord;

            public void Print(TrieTree n, int hight)
            {
                Console.WriteLine(hight + " " + n.Value + ": " + n.IsWord);
                foreach(KeyValuePair<char, TrieTree> entry in n.Edges)
                {
                    Print(entry.Value, hight + 1);
                }
            }        
            public static StringBuilder LongestSubString(TrieTree node, string str)
            {
                StringBuilder ans = new StringBuilder();
                StringBuilder localAns = new StringBuilder();
                localAns.Append('[');
                TrieTree t = node;
                for (int i = 0; i < str.Length; i++)
                {
                    if (t.Edges.ContainsKey(str[i]))
                    {
                        localAns.Append(str[i]);
                        if (t.Edges[str[i]].IsWord)
                        {
                            if (ans.Length < localAns.Length)
                                ans = new StringBuilder(localAns.ToString());
                        }
                        t = t.Edges[str[i]];
                    }
                    else
                    {
                        break;
                    }

                }
                if (ans.Length > 0)
                    ans.Append(']');
                return ans;
            }            
        }

        public static  String[] findSubstrings(String[] words, String[] parts)
        {
            var ans = new string[words.Length];
            TrieTree root = new TrieTree();
            TrieTree t = root;
            foreach (var part in parts)
            {
                t = root;
                for (int i = 0; i < part.Length; i++)
                {
                    if (t.Edges.ContainsKey(part[i]))
                    {
                        if (part.Length - 1 == i)
                            t.Edges[part[i]].IsWord = true;
                        t = t.Edges[part[i]];
                    }  else
                    {
                        var n = new TrieTree { Value = part[i], IsWord = part.Length - 1 == i };
                        t.Edges.Add(part[i], n);
                        t = n;
                    }
                }
            }

            for (int i = 0; i < words.Length; i++)            
                ans[i] = GenerateString(root, words[i]);
                        
            return ans;
        }

        private static string GenerateString(TrieTree root, string str)
        {
            StringBuilder ans = new StringBuilder();
            int maxCount = -1;

            for (int i = 0; i < str.Length; i++)
            {
                TrieTree t = root;
                for (int j = i; j < str.Length; j++)
                {
                    StringBuilder localMax = new StringBuilder(str.Substring(0, j));
                    StringBuilder tmp = TrieTree.LongestSubString(root, str.Substring(j));
                    //Console.WriteLine(tmp.ToString());                        
                    if (maxCount < tmp.Length)
                    {
                        maxCount = tmp.Length;
                        localMax.Append(tmp);
                        int k = tmp.Length > 0 ? tmp.Length - 2 + j : j;
                        localMax.Append(str.Substring(k));
                        ans = localMax;
                    }
                }
            }
            return ans.ToString();
        }

        public static void Main(string[] args)
        {
            // https://codefights.com/interview-practice/task/Ki9zRh5bfRhH6oBau
            Console.WriteLine("********************Start********************");

            Console.WriteLine("*********************End*********************");
        }
    }
}
