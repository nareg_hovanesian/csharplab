﻿using QuickGraph;
using QuickGraph.Algorithms;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpLab.Graph
{

    public class Program
    {      
        public static void Main(string[] args)
        {
            Console.WriteLine("********************Start********************");

            Graph<int> g = new Graph<int>();

            List<GraphNode<int>> nodesList = new List<GraphNode<int>>();

            IDictionary<int, GraphNode<int>> dic = new Dictionary<int, GraphNode<int>>();
            dic.Add(1, new GraphNode<int>(1));
            dic.Add(1, new GraphNode<int>(1));
            dic.Add(1, new GraphNode<int>(1));
            dic.Add(1, new GraphNode<int>(1));

            g.AddNode(new GraphNode<int>(1));
            g.AddNode(new GraphNode<int>(2));
            g.AddNode(new GraphNode<int>(3));
            g.AddNode(new GraphNode<int>(4));
            g.AddNode(new GraphNode<int>(5));

            //g.AddDirectedEdge(g.First(1), new GraphNode<int>(2), 0);
            g.AddDirectedEdge(new GraphNode<int>(0), new GraphNode<int>(1), 0);

            

            //g.addEdge(0, 2);
            //g.addEdge(1, 2);
            //g.addEdge(2, 0);
            //g.addEdge(2, 3);
            //g.addEdge(3, 3);
            foreach (var item in g.Nodes)
            {
                Console.WriteLine(item.Value);
            }


            //var edges = new SEdge<int>[] { new SEdge<int>(1, 2), new SEdge<int>(0, 1) };
            //var g = edges.ToAdjacencyGraph<int, SEdge<int>>(true);

            //g.AddEdge(new SEdge<int>(2, 3));
            //g.AddEdge(new SEdge<int>(1, 2));


            //foreach (var v in g.Vertices)
            //    Console.WriteLine(v);

            //foreach (var v in g.Edges)
            //    Console.WriteLine(v);

            //foreach (var v in g.Vertices)
            //    foreach (var e in g.OutEdges(v))
            //        Console.WriteLine(e);

            Console.WriteLine("*********************End*********************");
        }
    }
}
