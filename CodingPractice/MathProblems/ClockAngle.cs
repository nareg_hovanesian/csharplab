﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpLab.MathProblems
{
    public class ClockAngle
    {
        // https://www.youtube.com/watch?v=LFAhxzqvyps
        public double Alg(int hour,int min)
        {
            double M = min * (360 / 60);
            double H = hour * (360 / 12) + ((min * 1.0 / 60) * 30);
            double degree = Math.Abs(M - H);
            return (degree < 180) ? degree : 360 - degree;
        }
        public static void Main(string[] args)
        {
            Console.WriteLine("********************Start********************");
            var degree = new ClockAngle().Alg(1, 30);
            Console.WriteLine(degree);
            Console.WriteLine("*********************End*********************");
        }
    }
}
