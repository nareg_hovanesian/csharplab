﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpLab.MathProblems
{
    public class GetIntegerComplement
    {
        public static int getIntegerComplement(int n)
        {
            //Console.WriteLine("N: " + Convert.ToString(n, 2));
            string binaryN = Convert.ToString(n, 2);
            int filter = 1;
            for (int i = 0; i < binaryN.Length - 1; i++)            
                filter = filter << 1 | 1;                        
            return filter ^ n;
        }

        public static void Main(string[] args)
        {
            Console.WriteLine("********************Start********************");      
            var v = getIntegerComplement(100);
            Console.WriteLine(v);
            Console.WriteLine("*********************End*********************");
        }

    }
}
