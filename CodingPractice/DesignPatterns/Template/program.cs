﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpLab.DesignPatterns.Template
{
    class program
    {
        static void Main(string [] args)
        {
            new CalculateSum().Run();
        }
    }

    abstract class Calculate
    {
        public abstract void CalculateA();
        public abstract void CalculateB();

        public void Run()
        {
            CalculateA();
            CalculateB();
        }
    }

    class CalculateSum: Calculate
    {
        public override void CalculateA()
        {
            Console.WriteLine("Calculating A");
        }
        public override void CalculateB()
        {
            Console.WriteLine("Calculating B");
        }
    }



}
