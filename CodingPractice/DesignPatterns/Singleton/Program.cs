﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpLab.DesignPatterns.Singleton
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("********************Start********************");
            Console.WriteLine(Singleton.Instance.MyProperty++);
            Console.WriteLine(Singleton.Instance.MyProperty);
            Console.WriteLine("*********************End*********************");
        }
    }
    sealed class Singleton
    {
        //private static object syncRoot = new object();
        //private static volatile Singelton _instance;
        private static readonly Singleton _instance = new Singleton();
        public int MyProperty { get; set; }
        private Singleton() { MyProperty = 5; }

        // https://msdn.microsoft.com/en-us/library/ff650316.aspx
        //public static Singelton Instance
        //{
        //    get
        //    {
        //        if (_instance == null) { 
        //            lock (syncRoot)
        //            {
        //                if (_instance == null)
        //                {
        //                    _instance = new Singelton();
        //                }                        
        //            }
        //        }
        //        return _instance;
        //    }
        //}
        public static Singleton Instance
        {
            get
            {
                return _instance;
            }
        }


    }
    
}
