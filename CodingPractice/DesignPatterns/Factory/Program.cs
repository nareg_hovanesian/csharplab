﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpLab.DesignPatterns.Factory
{
    
    public class Program
    {
        public static void Main(string [] args)
        {
            Console.WriteLine("********************Start********************");

            var carFactory = new CarFactory();
            
            ICar bmw = carFactory.CreateCar(CarNames.BMW);
            ICar toyota = carFactory.CreateCar(CarNames.Toyota);

            Console.WriteLine($"Bmw Price is ${bmw.Price} ");
            Console.WriteLine($"Toyota Price is ${toyota.Price} ");

            Console.WriteLine("*********************End*********************");
        }

    }
    static class CarNames
    {
        public const string BMW = "BMW";
        public const string Toyota = "Toyota";
    }

    abstract class ICar
    {
        public decimal Price { get; set; }
    }

    class Toyota : ICar
    {
        public Toyota()
        {
            Price = 20000;
        }
    }

    class BMW : ICar
    {
        public BMW()
        {
            Price = 50000;
        }
    }

    interface ICarFactory
    {
        ICar CreateCar(string carModel);
    }

    class CarFactory : ICarFactory
    {
        public ICar CreateCar(string acctNo)
        {
            switch (acctNo)
            {
                case CarNames.BMW:
                    return new BMW();
                case CarNames.Toyota:
                    return new Toyota();
                default:
                    throw new ArgumentException("Invalid Account Number");
            }
        }
    }
}
