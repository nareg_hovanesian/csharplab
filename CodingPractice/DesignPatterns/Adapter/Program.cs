﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpLab.DesignPatterns.Adapter
{
    public class Program
    {
        // https://www.codeproject.com/Articles/774259/Adapter-Design-Pattern-Csharp
        public static void Main(string[] args)
        {
            Console.WriteLine("********************Start********************");
            var client = new Client(new ClassAdapter());
            client.Request();
            new Client(new ObjAdapter()).Request();
            Console.WriteLine("*********************End*********************");
        }
    }
    class Client
    {        
        public ITarget Target { get; set; }
        public Client(ITarget target)
        {
            Target = target;
        }
        public void Request()
        {
            Target?.MethodA();
        }
    }
    interface ITarget
    {
        void MethodA();   
    }
    class ClassAdapter : Adaptee, ITarget
    {
        public void MethodA()
        {
            MethodB();
        }
    }
    class ObjAdapter : ITarget
    {
        Adaptee _adaptee = new Adaptee();
        public void MethodA()
        {
            _adaptee.MethodB();
        }
    }
    class Adaptee
    {
        public void MethodB()
        {
            Console.WriteLine("MethodB got Called from Adaptee");
        }
    }
   
}
