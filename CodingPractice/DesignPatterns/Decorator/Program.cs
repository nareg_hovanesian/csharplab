﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpLab.DesignPatterns.Decorator
{
    class Program
    {
        // Wrapper Pattern         
        public static void Main(string[] args)
        {
            Console.WriteLine("********************Start********************");
            var car = new SeatWarmer(new Navigation(new CompactCar()));
            Console.WriteLine(car.GetDesc());
            Console.WriteLine($"Price of: ${car.GetCarPrice()} ");

            var car2 = new Navigation(new CompactCar());
            Console.WriteLine(car2.GetDesc());
            Console.WriteLine($"Price of: ${car2.GetCarPrice()} ");

            Console.WriteLine("*********************End*********************");
        }     
    }
    abstract class Car
    {
        protected string Desc { get; set; }
        public abstract string GetDesc();
        public abstract double GetCarPrice();
    }
    class CompactCar : Car
    {
        public CompactCar()
        {
            Desc = "Compact Car";
        }

        public override string GetDesc() => Desc;
        public override double GetCarPrice() => 1500.00;
    }
    class CarDecorator : Car
    {
        protected Car _car;

        public CarDecorator(Car car)
        {
            _car = car;
        }
        public override double GetCarPrice() => _car.GetCarPrice();

        public override string GetDesc() => _car.GetDesc();
    }
    class Navigation : CarDecorator
    {
        public Navigation(Car car) : base(car)
        {
            Desc = "Navigation";
        }
        public override string GetDesc() => $"{_car.GetDesc()}, {Desc}" ;
        public override double GetCarPrice() => _car.GetCarPrice() + 500;
    }
    class SeatWarmer : CarDecorator
    {
        public SeatWarmer(Car car) : base(car)
        {
            Desc = "Seat Warmer";
        }
        public override string GetDesc() => $"{_car.GetDesc()}, {Desc}";
        public override double GetCarPrice() => _car.GetCarPrice() + 800;
    }

}
