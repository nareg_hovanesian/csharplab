﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace CSharpLab.DesignPatterns.Observer
{
    public class Program2
    {
        public static EventHandlerList et;  // something to look at in for future 
        public static void Main(string[] args)
        {
            Console.WriteLine("********************Start********************");

            Logger.Singleton.Log += (e,o) => Console.WriteLine($"From Main log: \r\n {o._Msg}");

            Logger.Singleton.Log += (e, o) => Console.WriteLine($"From Main log 2: \r\n {o._Msg}");

            Logger.Singleton.AddMsg(1, "This is a test log with priority 1");
            Logger.Singleton.AddMsg(2, "This is a test log with priority 2");


            
        Console.WriteLine("*********************End*********************");
        }        
    }    

    public class LoggerEventArgs : EventArgs
    {

        public string _Msg { get; set; }
        public int _Priority { get; set; }
        public LoggerEventArgs(int Priority, string Msg)
        {
            _Priority = Priority;
            _Msg = Msg;
        }
    } 
    public class Logger
    {
        static Logger()
        {
            Singleton = new Logger();
        }
        private Logger() { }
        public static Logger Singleton { get; }
        public event EventHandler<LoggerEventArgs> Log;              
        public void AddMsg(int Priority, string Msg) => Log?.Invoke(this, new LoggerEventArgs(Priority, Msg));
    }
    class EventLogger
    {
        static EventLogger()
        {

        }
    }




}
