﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpLab.DesignPatterns.Observer
{       
    public enum FormState
    {
        CloseWithoutSave,
        SaveAndClose,
        Terminate
    }
    public class FormCloseEventArgs : EventArgs
    {
        public FormState CurrentFormState { get; set; }
        public FormCloseEventArgs(FormState formState)
        {
            CurrentFormState = formState;
        }
    }
    interface IRegularForm
    {
        void CloseForm(object sender, FormCloseEventArgs e);
    }

    public class RegularForm : IRegularForm
    {
        public string Name { get; set; }
        public RegularForm(MainForm mainForm)
        {
            mainForm.Close += CloseForm;    // subscribe to the mainform event
        }
        // sender will be the mainform
        public void CloseForm(object sender, FormCloseEventArgs e)
        {
            switch (e.CurrentFormState)
            {
                case FormState.CloseWithoutSave:
                    {
                        if (!((MainForm)sender).CancelClosing)
                        {
                            if (Error)
                            {
                                ((MainForm)sender).CancelClosing = true;
                                return;
                            }
                            Console.WriteLine("Closing " + Name);
                            ((MainForm)sender).Close -= this.CloseForm;
                        }

                    }
                    break;
                case FormState.SaveAndClose:
                    {
                        Console.WriteLine("Save and Closed " + Name);
                        ((MainForm)sender).Close -= this.CloseForm;
                    }
                    break;
            }
        }
        public bool Error { set; get; }
    }

    public class MainForm
    {
        private bool cancelClosing = false;
        private event EventHandler<FormCloseEventArgs> close;
        public event EventHandler<FormCloseEventArgs> Close
        {
            add
            {
                Console.WriteLine("Subscribing");
                close = value + close;  // adding the chain in reverse order
            }
            remove
            {
                Console.WriteLine("UnSubscribing");
                close -= value;
            }
        }
        public bool CancelClosing
        {
            get { return cancelClosing; }
            set { cancelClosing = value; }
        }
        public void CloseTheProgram()
        {
            Console.WriteLine("Closing MainForm");                            
            close?.Invoke(this, new FormCloseEventArgs(FormState.CloseWithoutSave));            
        }
        public MainForm() { }
    }

    public class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("********************Start********************");
            Console.WriteLine("Hello World");
            MainForm mainform = new MainForm();

            RegularForm f1 = new RegularForm(mainform);
            RegularForm f2 = new RegularForm(mainform);
            RegularForm f3 = new RegularForm(mainform);
            RegularForm f4 = new RegularForm(mainform);
            f2.Error = true;

            f1.Name = "f1";
            f2.Name = "f2";
            f3.Name = "f3";
            f4.Name = "f4";            
            mainform.CloseTheProgram();            
            mainform.CloseTheProgram();
            Console.WriteLine("*********************End*********************");
        }

    }


    

}
