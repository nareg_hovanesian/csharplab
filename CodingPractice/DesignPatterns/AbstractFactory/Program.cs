﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpLab.DesignPatterns.AbstractFactory
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("********************Start********************");

            var fac = new Client(new BMWFactory());
            Console.WriteLine(fac.Tire.Name);
            Console.WriteLine(fac.Engine.Name);

            fac = new Client(new ToyotaFactory());
            Console.WriteLine(fac.Tire.Name);
            Console.WriteLine(fac.Engine.Name);

            Console.WriteLine("*********************End*********************");
        }
    }

    interface ITire
    {
        string Name { get; }
    }
    interface IEngine
    {
        string Name { get; }
    }
    interface ICarFactory
    {
        ITire CreateTire();
        IEngine CreateEngine();
    }
    class ToyotaTire : ITire
    {        
        public string Name
        {
            get
            {
                return "Toyota Tire";
            }           
        }
    }
    class BMWTire : ITire
    {
        public string Name
        {
            get
            {
                return "BMW Tire";
            }
        }
    }
    class ToyotaEngine : IEngine
    {
        public string Name
        {
            get
            {
                return "Toyota Engine";
            }
        }
    }
    class BMWEngine : IEngine
    {
        public string Name
        {
            get
            {
                return "BMW Engine";
            }
        }
    }
    class BMWFactory : ICarFactory
    {
        public IEngine CreateEngine()
        {
            return new BMWEngine();
        }

        public ITire CreateTire()
        {
            return new BMWTire();
        }
    }
    class ToyotaFactory : ICarFactory
    {
        public IEngine CreateEngine()
        {
            return new ToyotaEngine();
        }

        public ITire CreateTire()
        {
            return new ToyotaTire();
        }
    }
    class Client
    {
        public ITire Tire { get; set; }
        public IEngine Engine { get; set; }
        public Client(ICarFactory facoty)
        {
            Engine = facoty.CreateEngine();
            Tire = facoty.CreateTire();
        }

    }
}
