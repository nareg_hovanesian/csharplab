﻿using System;

namespace CSharpLab.ArrayProblems
{
    public class RotateImage
    {
        // https://codefights.com/interview-practice/task/5A8jwLGcEpTPyyjTB
        // You are given an n x n 2D matrix that represents an image. Rotate the image by 90 degrees (clockwise).

        public static int[][] rotateImage(int[][] a)
        {
            //int k = a.Length % 2 == 0 ? a.Length / 2  : a.Length / 2 + 1;
            for (int i = 0; i < a.Length / 2; i++)
            {
                for (int j = 0; j < (a.Length + 1) / 2; j++)
                {
                    int RT = a[j][a.Length - i - 1];
                    a[j][a.Length - i-1] = a[i][j];

                    int RB = a[a.Length - i - 1][a.Length - j - 1];
                    a[a.Length - i - 1][a.Length - j - 1] = RT;

                    int LB = a[a.Length - j - 1][i];
                    a[a.Length - j - 1][i] = RB;
                    a[i][j] = LB;                  
                }
            }
            return a;
        }       

        public static void Main(string[] args)
        {
            Console.WriteLine("********************Start********************");
            //int[][] a = {
            //    new int [3] { 1, 2, 3},
            //    new int [3] { 4, 5, 6},
            //    new int [3] { 7, 8, 9}                
            //};
            int[][] a = {
                new int [4] { 1, 2, 3, 0},
                new int [4] { 4, 5, 6, 1},
                new int [4] { 7, 8, 9, 2},
                new int [4] { 7, 8, 9, 3}
            };

            var ans = rotateImage(a);
            foreach (var item in ans)
            {
                Console.WriteLine(" " + string.Join(", ", item));
            }            

            Console.WriteLine("*********************End*********************");
        }
    }
}
