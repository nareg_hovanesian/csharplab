﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpLab.ArrayProblems
{
    public class ThreeSum
    {
        // https://www.youtube.com/watch?v=-AMHUdZc9ss&t=651s
        public List<int[]> Alg(int [] set)
        {
            var ans = new List<int[]>();
            var list = set.OrderBy(a => a).Distinct().ToList();                                                
            for (int i = 0; i < list.Count() - 3; i++)
            {                
                int start = i + 1;
                int end = list.Count() - 1;
                while (start < end)
                {
                    if (list[i] + list[start] + list[end] == 0)
                        ans.Add(new int[] { list[i], list[start], list[end] });
                    if (list[i] + list[start] + list[end] < 0)                    
                        start++;
                    else                     
                        end--;                    
                }                                     
            }            
            return ans;
        }

        public static void Main(string[] args)
        {
            Console.WriteLine("********************Start********************");


            int[] set = { 101, 21, 22, 100, -122, 200, 300 };

           

            var res = new ThreeSum().Alg(set);
            foreach (var item in res)
            {
                Console.WriteLine("Coins: " + string.Join(", ", item));
            }
            

            Console.WriteLine("*********************End*********************");
        }
    }

}
