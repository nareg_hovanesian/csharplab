﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpLab.ArrayProblems
{
    public class ReverseStack
    {
        public Stack<T> Alg<T>(Stack<T> stack)
        {
            if (stack.Count == 0)
            {
                return stack;
            }
            T tmp = stack.Pop();
            Alg(stack);
            InsertToButtom(stack, tmp);
            return stack;
        }

        private void InsertToButtom<T>(Stack<T> stack, T val)
        {
            if (stack.Count == 0) { 
                stack.Push(val);
                return;
            }
            T tmp = stack.Pop();
            InsertToButtom(stack, val);
            stack.Push(tmp);
        }

        public static void Main(string[] args)
        {
            Console.WriteLine("********************Start********************");

            var stack = new Stack<int>(); 
            stack.Push(1);
            stack.Push(2);
            stack.Push(3);
            stack.Push(4);
            stack.Push(5);
            stack.Push(6);
            stack.Push(7);

            Console.WriteLine("Stack:    " + string.Join(", ", stack));
            var ans = new ReverseStack().Alg(stack);            
            Console.WriteLine("Reversed: " + string.Join(", ", ans));
            Console.WriteLine("*********************End*********************");
        }
    }
}
