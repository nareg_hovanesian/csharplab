﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpLab.ArrayProblems
{
    // http://www.geeksforgeeks.org/find-number-of-triangles-possible/
    public class NumberOfPossibleTriangles
    {        
        public int Alg(int[] arr)
        {
            int count = 0;
            Array.Sort(arr);
            
            for (int i = 0; i < arr.Length - 2; i++)
            {
                for (int j = i + 1; j < arr.Length; j++)
                {
                    int k = j + 1;
                    while (k < arr.Length && arr[k] < arr[i] + arr[j]) k++;

                    count += k - j - 1;
                }
            }
            return count;
        }
        
        public static void Main(string[] args)
        {
            Console.WriteLine("********************Start********************");

            var alg = new NumberOfPossibleTriangles();            
            Random random = new Random();

            int[] set = { 10, 21, 22, 100, 101, 200, 300 };

            Console.WriteLine("Coins: " + string.Join(", ", set));     
                              
            int res = alg.Alg(set);
           
            Console.WriteLine("NumberOfPossibleTriangles: " + res + ": " );
            Console.WriteLine("*********************End*********************");
        }
    }
}
