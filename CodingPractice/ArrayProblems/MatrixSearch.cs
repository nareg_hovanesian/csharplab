﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpLab.ArrayProblems
{
    // Not finished , needs more work
    public class MatrixSearch
    {
        public bool Alg(int[,] arr, int val)
        {
            return Search(arr, 0, arr.Length - 1, 0, arr.Length - 1, val);            
        }
        private bool Search(int[,] arr, int rs, int rf, int cs, int cf, int val)
        {
            if (rf > arr.Length|| cf > arr.GetUpperBound(1) || arr[rs,cs] < val || arr[rf,cf] > val)
                return false;
            if (val > arr[rs, cf])
                return Search(arr, rs+1, rf, cs, cf, val);
            if (val > arr[rs, cs])
                return Search(arr, rs, rf, cs + 1, cf, val);
                        
            return false;
        }

        public int? BinarySearch<T>(T[] arr, T val) where T : IComparable
        {
            return BinarySearch(arr, val, 0, arr.Length - 1);
        }
        private int ?  BinarySearch<T>(T [] arr, T val, int s, int f) where T : IComparable
        {
            if (s <= f)
            {                     
                int m = (s + f) / 2;            
                if(arr[m].CompareTo(val) < 0)
                    return BinarySearch(arr, val, m + 1, f);
                else if (arr[m].CompareTo(val) > 0)
                    return BinarySearch(arr, val,  s, m - 1);
                return m;
            }
            return null;
        }
        
        public static void Main(string[] args)
        {
            Console.WriteLine("********************Start********************");
            int[] list = new int[] { 1 };

            var v = new MatrixSearch().BinarySearch(list, 3);
            Console.WriteLine("ans: " + v );

            int[,] arr = new int[3, 4] {
               {0, 2, 3, 8 },
               {6, 7, 14, 15 },
               {9, 12, 17, 22 }
            };
            Console.WriteLine(new MatrixSearch().Alg(arr, 2));

            Console.WriteLine("*********************End*********************");
        }
    }
}
