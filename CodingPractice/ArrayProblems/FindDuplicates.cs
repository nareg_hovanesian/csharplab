﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpLab.ArrayProblems
{
    // https://www.youtube.com/watch?v=GeHOlt_QYz8
    // find all duplicates in array, where for any elemnt x, 1 <= x <= list.Count()
    public class FindDuplicates
    {
        public List<int> Alg(int [] list)
        {
            var ans = new List<int>();
            
            for (int i = 0; i < list.Count(); i++)
            {
                int index = Math.Abs(list[i]) - 1;
                if (list[index] < 0)
                {
                    ans.Add(Math.Abs(list[i]));
                }
                else  
                {
                    list[index] = -list[index];
                }                
            }
            for (int i = 0; i < list.Length; i++)
            {
                list[i] = Math.Abs(list[i]);
            }
            return ans;
        }

        // Second problem :
        // Google interview example 
        // Given an array a that contains only numbers in the range from 1 to a.length, find the first duplicate number for which the second
        // occurrence has the minimal index. In other words, if there are more than 1 duplicated numbers, 
        // return the number for which the second occurrence has a smaller index than the second occurrence of the other number does.
        // If there are no such elements, return -1.
        //  [2, 3, -3, -1, 5, 2]
        // https://codefights.com/interview-practice/task/pMvymcahZ8dY4g75q
        public static int firstDuplicate(int[] a)
        {
            for (int i = 0; i < a.Length; i++)
            {
                if (a[Math.Abs(a[i]) - 1] < 0)
                    return Math.Abs(a[i]);
                a[Math.Abs(a[i]) - 1] = -a[Math.Abs(a[i]) - 1];
            }
            return -1;
        }

        public static void Main(string[] args)
        {
            Console.WriteLine("********************Start********************");

            int[] set = { 1, 2, 5, 3, 2, 2, 1, 5, 3, 6 };
            var ans = new FindDuplicates().Alg(set);
            Console.WriteLine("Duplicates: " + string.Join(", ", ans));

            Console.WriteLine("*********************End*********************");
        }
    }
}
