﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpLab.ArrayProblems
{
    public class StackSort
    {
        // https://www.youtube.com/watch?v=nll-b4GeiX4
        public Stack<T> Alg<T>(Stack<T> st) where T :IComparable
        {
            Stack<T> res = new Stack<T>();
            if (st == null && st.Count == 0)
                return res;
    
            while (st.Count > 0)
            {
                T tmp = st.Pop();
                //if ( res.Count == 0 || tmp.CompareTo(res.Peek()) <= 0)
                //    res.Push(tmp);
                //else {
                    while (res.Count > 0 && tmp.CompareTo(res.Peek()) >= 0) { st.Push(res.Pop()); }
                    res.Push(tmp);
                //}
            }

            return res;
        }

        public static void Main(string[] args)
        {
            Console.WriteLine("********************Start********************");


            var stack = new Stack<int>(); // 4,7,1,2,6,5,3
            stack.Push(3);  
            stack.Push(5);
            stack.Push(6);
            stack.Push(2);
            stack.Push(1);
            stack.Push(7);
            stack.Push(4);

            // 1, 2, 3, 4, 5, 6
            // 2, 1, 3, 4, 5, 6
            // 
            // t: 1 
            // b: 2

            var ans = new StackSort().Alg(stack);
            Console.WriteLine("Duplicates: " + string.Join(", ", ans));

            Console.WriteLine("*********************End*********************");
        }
    }
}
