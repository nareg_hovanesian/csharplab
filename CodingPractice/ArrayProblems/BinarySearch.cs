﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingPractice.ArrayProblems
{
    // 1 http://www.geeksforgeeks.org/binary-search/

    // 2 http://www.geeksforgeeks.org/count-number-of-occurrences-or-frequency-in-a-sorted-array/
    public class BinarySearch
    {        
        public int Alg(int[] arr, int num)
        {
            return binarySearch(arr, 0, arr.Length - 1, num);             
            //return binarySearch(arr, num);
        }
      
        public int Partition(int[] arr, int l, int r)
        {
            int p = arr[r];
            int j = l;
            for (int i = l; i <= r - 1; i++)
            {
                if (arr[i] < p)
                {
                    swap(arr, i, j);
                    j++;
                }
            }
            swap(arr, j, r);
            return j;
        }

        public int binarySearch(int [] arr, int l, int r, int num)
        {
            if (l > r)
                return -1;            
            int m = (r + l) / 2;                        
            if (arr[m] < num)
                return binarySearch(arr, m + 1, r, num);
            if (arr[m] > num)
                return binarySearch(arr, l, m-1, num);
            return m;
        }

        public int BinarySearchFirst(int[] arr, int l, int r, int x)
        {
            if (l > r)
                return -1;
            int m = (l + r) / 2;
            if ((m == 0 || arr[m - 1] != x) && arr[m] == x)
                return m;
            if (arr[m] < x)
                return BinarySearchFirst(arr, m + 1, r, x);
            else
                return BinarySearchFirst(arr, l, m - 1, x);            
        }

        public int BinarySearchLast(int[] arr, int l, int r, int x)
        {
            if (l > r)
                return -1;
            int m = (l + r) / 2;
            if ((m == arr.Length - 1 || arr[m + 1] != x) && arr[m] == x)
                return m;
            if (arr[m] < x)
                return BinarySearchFirst(arr, m + 1, r, x);
            else
                return BinarySearchFirst(arr, l, m - 1, x);
        }


        public int binarySearch(int[] arr, int num)
        {
            int l = 0;            
            int r = arr.Length - 1;
            while (l <= r)
            {
                int m = (l + r) / 2;
                if (arr[m] == num)                
                    return  m;                    
                if (arr[m] < num)
                    l = m + 1;
                else
                    r = m - 1;
            }
            return -1;
        }

        public void swap(int [] arr, int i, int j)
        {
            int temp = arr[i];
            arr[i] = arr[j];
            arr[j] = temp;
        }      

        public int FrequencySearch(int [] arr, int num)
        {
            int ans = 0;


            int i = binarySearch(arr, 0, arr.Length - 1, ans);


            return ans;
        }
       
        public static void Main(string[] args)
        {
            Console.WriteLine("********************Start********************");

            var alg = new BinarySearch();
            int min = -100;
            int max = 100;
            Random random = new Random();

            int[] set = Enumerable
                .Repeat(0, 11)
                .Select(i => random.Next(min, max))
                .ToArray();

            Array.Sort(set);
            Console.WriteLine("Coins: " + string.Join(", ", set));
            int k = random.Next(0, 10);          
            int res = alg.Alg(set, set[k]);
            
            Console.WriteLine("Binary Search " + set[k]  + ": " + string.Join(", ", res) + " ans:" + k);                        
            Console.WriteLine("*********************End*********************");
        }
    }
}
