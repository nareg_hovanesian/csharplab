﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpLab.ArrayProblems
{
    public class Sudoku2
    {
        // Sudoku is a number-placement puzzle. The objective is to fill a 9 × 9 grid with numbers in such a way that each column, 
        // each row, and each of the nine 3 × 3 sub-grids that compose the grid all contain all of the numbers from 1 to 9 one time.
        // https://codefights.com/interview-practice/task/SKZ45AF99NpbnvgTn
     
        // 
        public static bool sudoku2(char[][] grid)        
        {
            for (int i = 0; i < grid.Length; i++)
                for (int j = 0; j < grid[0].Length; j++)
                {
                    if (grid[i][j] == '.')
                        continue;
                    for (int k = 0; k < grid.Length; k++)
                    {
                        if (grid[i][k] == grid[i][j] && j != k)   // search row 
                            return false;
                        if (grid[k][j] == grid[i][j] && i != k)
                            return false;
                    }
                    int r = 0;
                    int f = 0;

                    if (i < 3) r = 0;
                    else if (i < 6) r = 3;
                    else r = 6;

                    if (j < 3) f = 0;
                    else if (j < 6) f = 3;
                    else f = 6;
                    int rl = r + 3;
                    int fl = f + 3;
                    for (; r < rl; r++)
                        for (; f < fl; f++)
                            if (grid[r][f] == grid[i][j] && i != r & f != j)
                                return false;
                }
            return true;
        }

        // more readable version: written by shouki         
        // https://codefights.com/interview-practice/task/SKZ45AF99NpbnvgTn/solutions/iRyvkpsi5F8bZvwyD
        public static bool sudoku3(char[][] grid)
        {
            if (grid.Any(row => IsInvalid(row)) || // across rows
                grid.Select((_, i) => i).Any(c => IsInvalid(grid.Select(_ => _[c])))) // down columns
                return false;

            // within sub-grids
            for (int r = 0; r < grid.Length; r += 3)
            {
                for (int c = 0; c < grid.Length; c += 3)
                {
                    if (IsInvalid(grid.Skip(r).Take(3).SelectMany(_ => _.Skip(c).Take(3))))
                        return false;
                }
            }
            return true;
        }

        public static bool IsInvalid(IEnumerable<char> numbers)
        {
            var counts = new int[9];
            return numbers.Any(n => n != '.' && counts[n - '1']++ > 0);
        }

        public static void Main(string[] args)
        {
            Console.WriteLine("********************Start********************");

            char[][] grid = {
                new char[9]{'.', '.', '.', '1', '4', '.', '.', '2', '.'},
                new char[9]{'.', '.', '6', '.', '.', '.', '.', '.', '.'},
                new char[9]{'.', '.', '.', '.', '.', '.', '.', '.', '.'},
                new char[9]{'.', '.', '1', '.', '.', '.', '.', '.', '.'},
                new char[9]{'.', '6', '7', '.', '.', '.', '.', '.', '9'},
                new char[9]{'.', '.', '.', '.', '.', '.', '8', '1', '.'},
                new char[9]{'.', '3', '.', '.', '.', '.', '.', '.', '6'},
                new char[9]{'.', '.', '.', '.', '.', '7', '.', '.', '.'},
                new char[9]{'.', '.', '.', '5', '.', '.', '.', '7', '.'}                
            };
            var ans = sudoku3(grid);
            Console.WriteLine("Ans: " +  ans);
            int a = '2' - '1';
            Console.WriteLine('1' - '1');
            Console.WriteLine(a);
            Console.WriteLine("*********************End*********************");
        }
    }
}
