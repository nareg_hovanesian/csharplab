﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpLab.ArrayProblems
{
    public class NonRepeatedCharacter
    {
        // https://www.youtube.com/watch?v=GJdiM-muYqc
        public static string nonRepeated(string s)
        {
            int[] set = new int[256];
            for (int i = 0; i < s.Length; i++)
            {
                set[(int)s[i]]++;
            }
            for (int i = 0; i < s.Length; i++)
            {
                if (set[s[i]] == 1)
                    return s[i].ToString();
            }
            return "";
        }
        public static void Main(string[] args)
        {
            Console.WriteLine("********************Start********************");
            var v = nonRepeated("mnomn");                                    
            Console.WriteLine(v);            
            Console.WriteLine("*********************End*********************");
        }
    }
}
