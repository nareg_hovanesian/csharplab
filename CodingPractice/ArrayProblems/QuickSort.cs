﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpLab.ArrayProblems
{
    // http://www.geeksforgeeks.org/rearrange-positive-and-negative-numbers-publish/

    // Also check this relative question : http://www.geeksforgeeks.org/rearrange-positive-and-negative-numbers/
    public class QuickSort
    {        
        public int[] Alg(int[] arr)
        {
            Sort(arr, 0, arr.Length - 1);
            return arr;
        }
        //  1 2 3 4
        //  
        public int Partition(int[] arr, int l, int r)
        {
            int p = arr[r];
            int j = l;
            for (int i = l; i <= r - 1; i++)
            {
                if (arr[i] < p)
                {
                    swap(arr, i, j);
                    j++;
                }
            }
            swap(arr, j, r);
            return j;
        }

        public void Sort(int [] arr, int l, int r)
        {
            if(l < r)
            {
                int p = Partition(arr, l, r);
                Sort(arr, l, p-1);
                Sort(arr, p+1, r);                
            }        
        }

        public void swap(int [] arr, int i, int j)
        {
            int temp = arr[i];
            arr[i] = arr[j];
            arr[j] = temp;
        }      

        public bool CheckSort(int [] arr)
        {
            for (int i = 0; i < arr.Length -  1; i++)
            {
                if(arr[i] > arr[i + 1])
                {
                    return false;
                }
            }
            return true;
        }
        public static void Main(string[] args)
        {
            Console.WriteLine("********************Start********************");

            var alg = new QuickSort();
            int min = -100;
            int max = 100;
            Random random = new Random();

            int[] set = Enumerable
                .Repeat(0, 20)
                .Select(i => random.Next(min, max))
                .ToArray();

            Console.WriteLine("Coins: " + string.Join(", ", set));     
                              
            int[] res = alg.Alg(set);
           
            Console.WriteLine("QuickSort: " + string.Join(", ", res) + ": " + alg.CheckSort(res).ToString());
            Console.WriteLine("*********************End*********************");
        }
    }
}
