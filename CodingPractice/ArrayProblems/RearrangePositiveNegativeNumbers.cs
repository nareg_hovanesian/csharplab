﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpLab.ArrayProblems
{
    // http://www.geeksforgeeks.org/rearrange-positive-and-negative-numbers-publish/

    // Also check this relative question : http://www.geeksforgeeks.org/rearrange-positive-and-negative-numbers/
    public class RearrangePositiveNegativeNumbers
    {        
        public int[] Alg(int[] arr)
        {
            int i = -1;
            for (int j = 0; j < arr.Length; j++)
            {
                if (arr[j] < 0)
                {
                    i++;
                    swap(arr, i, j);         
                }
            }
            int Pos = i + 1;
            int Neg = 0;
            while (Pos < arr.Length && Neg < Pos && arr[Neg] < 0)
            {
                swap(arr, Neg, Pos);
                Neg += 2;
                Pos++;
            }                      
            return arr; 
        }
        public void swap(int [] arr, int i, int j)
        {
            int temp = arr[i];
            arr[i] = arr[j];
            arr[j] = temp;
        }      
        public static void Main(string[] args)
        {
            Console.WriteLine("********************Start********************");

            var alg = new RearrangePositiveNegativeNumbers();
            int[] set = { -1, -2, -3, -4, -5, -6, -7, -8, -9, -10, 11, 12, 13, 14 };
            //Console.WriteLine("Coins: " + string.Join(", ", set));            
            int[] res = alg.Alg(set);
            Console.WriteLine("Coins: " + string.Join(", ", res));
            Console.WriteLine("*********************End*********************");
        }
    }
}
