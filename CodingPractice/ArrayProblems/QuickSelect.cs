﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpLab.ArrayProblems
{
    // 
    public class QuickSelect
    {        
        public int Alg(int[] arr, int ith)
        {
            Sort(arr, 0, arr.Length - 1, ith);
            return arr[ith];
            
        }
        //  1 2 3 4
        //  
        public int Partition(int[] arr, int l, int r)
        {
            int p = arr[r];
            int j = l;
            for (int i = l; i <= r - 1; i++)
            {
                if (arr[i] < p)
                {
                    swap(arr, i, j);
                    j++;
                }
            }
            swap(arr, j, r);
            return j;
        }

        public void Sort(int [] arr, int l, int r, int ith)
        {
            if(l < r)
            {
                int p = Partition(arr, l, r);
                if (ith == p)
                    return;
                if(ith < p)
                    Sort(arr, l, p-1, ith);
                if(ith > p)
                    Sort(arr, p+1, r, ith);                
            }        
        }

        public void swap(int [] arr, int i, int j)
        {
            int temp = arr[i];
            arr[i] = arr[j];
            arr[j] = temp;
        }      

        public bool CheckSort(int [] arr)
        {
            for (int i = 0; i < arr.Length -  1; i++)
            {
                if(arr[i] > arr[i + 1])
                {
                    return false;
                }
            }
            return true;
        }
        public static void Main(string[] args)
        {
            Console.WriteLine("********************Start********************");

            var alg = new QuickSelect();
            int min = -100;
            int max = 100;
            Random random = new Random();

            int[] set = Enumerable
                .Repeat(0, 10)
                .Select(i => random.Next(min, max))
                .ToArray();

            Console.WriteLine("Coins: " + string.Join(", ", set));     
                              
            int res = alg.Alg(set, 5);
            Array.Sort(set);
            Console.WriteLine("QuickSelect 5: " + string.Join(", ", res) + " ans:" + set[5]);
            
            
            Console.WriteLine("*********************End*********************");
        }
    }
}
