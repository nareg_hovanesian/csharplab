﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpLab.ArrayProblems
{
    // http://www.geeksforgeeks.org/rearrange-positive-and-negative-numbers-publish/

    // Also check this relative question : http://www.geeksforgeeks.org/rearrange-positive-and-negative-numbers/
    public class MergeSort
    {        
        public int[] Alg(int[] arr)
        {
            Sort(arr, 0, arr.Length - 1);
            return arr;
        }
        //  1 2 3 4
        //  
        public void Merge(int[] arr, int l, int m, int r)
        {
            int e1 = m - l + 1;
            int e2 = r - m;
            int[] ar1 = new int [e1];
            int[] ar2 = new int[e2];
            int i = 0, j = 0;
            while (i < e1)
            {
                ar1[i] = arr[l + i];
                i++;
            }
            while (j < e2)
            {
                ar2[j] = arr[m + j + 1];
                j++;
            }
            int k = l;
            i = 0; j = 0; 

            while(i < e1 && j < e2)            
                if(ar1[i] < ar2[j])                
                    arr[k++] = ar1[i++];                    
                else
                    arr[k++] = ar2[j++];                                                    
            
            while (i < e1)            
                arr[k++] = ar1[i++];                
            
            while (j < e2)            
                arr[k++] = ar2[j++];            
        }

        // 1 2 3 4 5 
        // l = 0 , r = 4, m = 2
        // 
        public void Sort(int [] arr, int l, int r)
        {
            if(l < r)
            {
                int m = (r + l) / 2;
                Sort(arr, l, m);
                Sort(arr, m + 1, r);
                Merge(arr, l, m, r);
            }        
        }

        public void swap(int [] arr, int i, int j)
        {
            int temp = arr[i];
            arr[i] = arr[j];
            arr[j] = temp;
        }      
        public static void Main(string[] args)
        {
            Console.WriteLine("********************Start********************");

            var alg = new MergeSort();
            int[] set = { -1, -2, -3, -4, -5, -6, -7, -8, -9, -10, 11, 12, 13, 14 };
            //Console.WriteLine("Coins: " + string.Join(", ", set));            
            int[] res = alg.Alg(set);
            Console.WriteLine("Coins: " + string.Join(", ", res));
            Console.WriteLine("*********************End*********************");
        }
    }   

}
