﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpLab.LinkedListPorblems
{
    // https://www.youtube.com/watch?v=i7v1UWlaYrI&list=PLNmW52ef0uwsjnM06LweaYEZr-wjPKBnj&index=2
    public class NthToLastLinkListElement
    {
        public class Node<T>
        {
            public T Value;
            public Node<T> Next;
            public Node(T val)
            {
                Value = val;
            }
        }

        public class LinkList<T>    
        {
            public Node<T> Root;
        }
      

        public Nullable<T>  Alg<T>(LinkList<T> list, int index) where T: struct
        {
            Node<T> curr = list.Root;
            Node<T> follower = list.Root;
            if (follower == null)
                return null;
            int i = 0;
            while (curr.Next != null)
            {
                if(i >= index)
                {
                    follower = follower.Next;
                }
                i++;
                curr = curr.Next;                
            }                                       
            return follower.Value;
        }
        public static void Main(string[] args)
        {
            Console.WriteLine("********************Start********************");            

            LinkList<int> list = new LinkList<int>();
            list.Root = new Node<int>(1);
            list.Root.Next = new Node<int>(2);
            list.Root.Next.Next = new Node<int>(3);
            list.Root.Next.Next.Next = new Node<int>(4);
            list.Root.Next.Next.Next.Next = new Node<int>(5);

            var ans = new NthToLastLinkListElement().Alg(list, 4);

            Console.WriteLine("Duplicates: " + ans);

            Console.WriteLine("*********************End*********************");
        }
    }
}
