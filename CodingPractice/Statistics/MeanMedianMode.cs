﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpLab.Statistics
{
    public class MeanMedianMode
    {
     
        // https://www.hackerrank.com/challenges/s10-basic-statistics
        public static void Main(string[] args)
        {
            double mean = 0;
            double median = 0;
            double mode = 0;

            var len = int.Parse(Console.ReadLine());
            int[] numbers = new int[len];
            numbers = Console
                     .ReadLine()
                     .Split(new Char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)
                     .Select(item => int.Parse(item))
                     .ToArray();

            if (numbers.Count() > 0)
            {
                mean = (numbers.Sum(v => v) * 1.0) / (numbers.Count() * 1.0);
                numbers = numbers.OrderBy(v => v).ToArray();
                if (numbers.Count() == 1)
                {
                    median = numbers[0];
                }
                else
                {
                    median = numbers.Count() % 2 > 0 ?
                        numbers[numbers.Count() / 2] :
                        (numbers[numbers.Count() / 2] * 1.0 + numbers[numbers.Count() / 2 - 1] * 1.0) / 2;
                }
                int popularCount = 1;
                int localPopular = 0;
                mode = numbers[0];
                int prev = numbers[0];
                
                foreach (var n in numbers)
                {
                    localPopular = (n == prev) ? localPopular + 1 : 1;
                    if (localPopular > popularCount)
                    {
                        popularCount = localPopular;
                        mode = n;
                    }
                    prev = n;
                }
            }
            
            Console.WriteLine(Math.Round(mean, 1));
            Console.WriteLine(Math.Round(median, 1));
            Console.WriteLine(mode);
        }
        
    }
}