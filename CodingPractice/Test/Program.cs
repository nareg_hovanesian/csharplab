﻿using QuickGraph;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpLab.Test
{       
    // Observer
    class OurEventArgs : EventArgs { }
    class Observable
    {
        public event EventHandler<OurEventArgs> Call;
        public void CallEveryone()
        {
            Call?.Invoke(this, new OurEventArgs());
        }
    }
    class Observer
    {
        Observable ob;
        public Observer(Observable o)
        {
            ob = o;
            ob.Call += (e, d) => { Console.WriteLine("Notified"); };
        }
    }
    public class Program
    {
        static public void Main(string[] args)
        {           
            Observable ob = new Observable();
            Observer o = new Observer(ob);
            Observer o1 = new Observer(ob);
            Observer o2 = new Observer(ob);
            ob.CallEveryone();
        }
    }
}
