﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpLab
{
    // How to run certain Main:
    // https://stackoverflow.com/questions/17607951/is-it-possible-to-have-more-than-one-main-method-in-a-c-sharp-program
    // Note: How to setup, so visual studio doesn't print in console 
    // https://stackoverflow.com/questions/2542599/having-the-output-of-a-console-application-in-visual-studio-instead-of-the-conso
    class Program
    {
        static void Main(string[] args)
        {
        }
    }
}
