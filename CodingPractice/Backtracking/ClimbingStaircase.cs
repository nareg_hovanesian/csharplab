﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpLab.Backtracking
{
    public class ClimbingStaircase
    {
        // https://codefights.com/interview-practice/task/cAXEnPyzknC5zgd7x/description
        // You need to climb a staircase that has n steps, and you decide to get some extra exercise by jumping up the steps.
        // You can cover at most k steps in a single jump.Return all the possible sequences of jumps that you could take to climb the staircase, sorted.
        public static int[][] climbingStaircase(int n, int k)
        {
            List<List<int>> list = new List<List<int>>();            
            climbing(n, k, new List<int>() , list);
            int[][] ans = new int[list.Count][];            
            ans = list.Select(a => a.ToArray()).ToArray();
            return ans;
        }
                
        public static void climbing(int n, int k, List<int> list, List<List<int>> lists)
        {
            if (n <=  0)
            {
                lists.Add(list);
                return;
            }                            
            for (int i = 1; i <= k; i++)
            {
                if( n - i >= 0)
                {
                    var lst = new List<int>(list);                   
                    lst.Add(i);                                                                
                    climbing(n - i, k, lst, lists);                    
                }                
            }         
        }

        public static void Main(string[] args)
        {
            Console.WriteLine("********************Start********************");

            var ans = climbingStaircase(7, 3);
            foreach (var item in ans)            
                Console.WriteLine(string.Join(",", item));             

            Console.WriteLine("*********************End*********************");
        }

    }
}
