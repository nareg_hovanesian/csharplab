﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpLab.Backtracking
{
    public class WordBoggle
    {
        // Boggle is a popular word game in which players attempt to find words in sequences of adjacent letters on a rectangular board.
        // Given a two-dimensional array board that represents the character cells of the Boggle board and an array of unique strings words, 
        // find all the possible words from words that can be formed on the board.
        public static string[] wordBoggle(char[][] board, string[] words)
        {
            return new string[3];
        }
        public static void Main(string[] args)
        {
            Console.WriteLine("********************Start********************");
            char[][] board = new char[3][]{ 
               new char[3] {'R', 'L', 'D'},
               new char[3] {'U', 'O', 'E'},
               new char[3] {'C', 'S', 'O'}
            };
            string[] words = { "CODE", "SOLO", "RULES", "COOL" };
            var ans = wordBoggle(board, words);
            foreach (var item in ans)
                Console.WriteLine(string.Join(",", item));

            Console.WriteLine("*********************End*********************");
        }
    }
}
