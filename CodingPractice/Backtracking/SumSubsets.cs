﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpLab.Backtracking
{
    // https://codefights.com/interview-practice/task/uJvihuafm7PRDPCsL
    // Given a sorted array of integers arr and an integer num, find all possible unique subsets of arr that add up to num. 
    // Both the array of subsets and the subsets themselves should be sorted in lexicographical order.
    public class SumSubsets
    {
        public class ListComparer : IComparer<List<int>>
        {
            public int Compare(List<int> x, List<int> y)
            {                
                for (int i = 0; i < Math.Max(x.Count(), y.Count()); i++)
                {
                    if (x[i] != y[i])
                        return x[i].CompareTo(y[i]);
                }
                return x.Count().CompareTo(y.Count());
            }
        }
        public static int[][] sumSubsets(int[] arr, int num)
        {
            List<List<int>> list = new List<List<int>>();
            for (int j = 1; j <= arr.Count(); j++)
                sumSubsetsHelper(list, new List<int>(), arr, num, 0, j);
            if(list.Count == 0)
                list.Add(new List<int>());                              
            list = list.OrderBy(
                q => q, 
                Comparer<List<int>>.Create(                
                    ( x, y) =>
                    {
                        for (int i = 0; i < Math.Max(x.Count(), y.Count()); i++)
                        {
                            if (x[i] != y[i])
                                return x[i].CompareTo(y[i]);
                        }
                        return x.Count().CompareTo(y.Count());
                    })                
                ).ToList();                        
            var ans= list.Select(q => q.ToArray()).ToArray();
            return ans;
        }
                        
        public static void sumSubsetsHelper(List<List<int>> lists, List<int> list, int [] arr, int num, int L, int len)
        {
            int s = list.Sum();
            if (len == 0)
            {
                if (s == num) {
                    list = list.OrderBy(q => q).ToList();
                    if(!lists.Any(q => q.SequenceEqual(list)))
                        lists.Add(list);
                }
                return;
            }
            if (s > num)
                return;
            for (int i = L; i <= arr.Count() - len; i++)
            {
                if (s + arr[i] > num)
                    break;
                var tmp = new List<int>(list);
                tmp.Add(arr[i]);
                sumSubsetsHelper(lists, tmp, arr, num, i + 1, len - 1);
            }
        }
        public static void Main(string[] args)
        {
            Console.WriteLine("********************Start********************");
            int [] arr = new int [10] { 1, 1, 2, 2, 2, 5, 5, 6, 8, 8 };
            var ans = sumSubsets(arr, 9);
            foreach (var item in ans)
                Console.WriteLine(string.Join(",", item));

            Console.WriteLine("*********************End*********************");
        }
    }
}
