﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpLab.Backtracking
{    
    public class NQueens
    {

        // https://codefights.com/interview-practice/task/7u7oKqXoFdmh3vGyb
        // In chess, queens can move any number of squares vertically, horizontally, or diagonally.
        // The n-queens puzzle is the problem of placing n queens on an n × n chessboard so that no two queens can attack each other.
        // Given an integer n, print all possible distinct solutions to the n-queens puzzle.
        // Each solution contains distinct board configurations of the placement of the n queens, 
        // where the solutions are arrays that contain permutations of[1, 2, 3, .. n].
        // The number in the ith position of the results array indicates that the ith column queen is placed in the row with that number.
        // In your solution, the board configurations should be returned in 
        public static int[][] nQueens(int n)
        {
            List<int[]> list = new List<int[]>();

            nQueensHelper(0, 0, new int[n], list);
            int[][] ans = new int[list.Count][];            
            ans = list.Select(a => a.ToArray()).ToArray();
            return ans;
        }
        public static void nQueensHelper(int n, int m, int [] list, List<int[]> lists)
        {
            if (n == list.Length)
            {
                lists.Add(list);
                return;
            }        
            for (int i = 0; i < list.Length; i++)
            {
                if (IsValid(list, n, i+1))
                {
                    var lst = new int[list.Length];
                    Array.Copy(list, lst, list.Length);
                    lst[n] = i + 1;
                    nQueensHelper(n + 1, i, lst, lists);
                }                
            }
        }
        public static bool IsValid(int[] list,int n, int m)
        {
            for (int i = 0; i < n; i++)
            {
                if (list[i] == m)
                    return false;
                int j = n - i;
                if (list[i] + j == m || list[i] - j  == m)
                    return false;
            }
            return true;
        }
        public static void Main(string[] args)
        {
            Console.WriteLine("********************Start********************");

            var ans = nQueens(4);
            foreach (var item in ans)
                Console.WriteLine(string.Join(",", item));

            Console.WriteLine("*********************End*********************");
        }
    }
}
