﻿using QuickGraph;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpLab.OOP.ReturnReferenceTypes
{
    public class Program
    {        
        public static void Main(string[] args)
        {
            Console.WriteLine("********************Start********************");

            Business business = new Business();

            var list = business.GetImportantData;
            list.ToList().ForEach((i) => Console.WriteLine(i.SecretNum));            
            list.First().SecretNum = 5;
            list.First().SecretStr = "Change Secret ";            
            foreach (var item in list)
            {
                Console.WriteLine(item.SecretNum + " " + item.SecretStr);
            }
                        
            business.GetImportantData.ToList().ForEach( (i)=> Console.WriteLine(i.SecretNum + " " + i.SecretStr) );

            business.GetSecrets.ToList().ForEach( (i) => Console.WriteLine(i));
            business.GetSecretStr.ToList().ForEach((i) => Console.WriteLine(i));

            var graph = new AdjacencyGraph<int, Edge<int>>();

            Console.WriteLine("*********************End*********************");
        }
    }

    class Data
    {
        public int SecretNum { get; set; }        
        public string SecretStr { get; set; }
    }
    class Business
    {
        public Business()
        {
            ImportantData.Add(new Data { SecretNum = 4, SecretStr = "Oops" });
        }
        private List<Data> ImportantData = new List<Data>();
        public IReadOnlyCollection<Data> GetImportantData { get { return new ReadOnlyCollection<Data>(ImportantData); } }

        public IEnumerable<int> GetSecrets
        {
           get
            {                
                var list = from i in ImportantData
                           select i.SecretNum;
                return list.ToList();
            }
        }

        public IEnumerable<string> GetSecretStr
        {
            get
            {
                var list = from i in ImportantData
                           select i.SecretStr;
                return list.ToList();
            }
        }
    }


}
