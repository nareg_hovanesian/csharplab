﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpLab.OOP.Covariance
{
    public class Program
    {
        // https://www.youtube.com/watch?v=JV_v3dLLNug
        public class Base { }
        public class Drive :Base { }

        delegate Base ReturnBaseDelegate();
        delegate Drive ReturnDriveDelegate();
        
        public static Base ReturnBase() { return null; }
        public static Drive ReturnDrive() { return null; }

        public static void Test(int [] list)
        {
            list[0] = 3;
        }

        public static void Main(string [] args)
        {
            ReturnBaseDelegate b1 = ReturnBase;
            ReturnBaseDelegate b2 = ReturnDrive;

            //ReturnDriveDelegate d1 = ReturnBase; won't work 
            ReturnDriveDelegate d2 = ReturnDrive;

            int[] list = new int[2] { 0 , 1 };

            Test(list);

            foreach (var item in list)
            {
                Console.WriteLine(item);
            }


        }
    }
}
