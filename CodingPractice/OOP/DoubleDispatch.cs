﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpLab.OOP
{
    // https://www.wikiwand.com/en/Double_dispatch#/Double_dispatch_is_more_than_function_overloading
    // https://stackoverflow.com/questions/479923/is-c-sharp-a-single-dispatch-or-multiple-dispatch-language
    // http://www.egr.unlv.edu/~matt/teaching/CSC460/Espresso.pdf
    class DoubleDispatch
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("********************Start********************");

            Asteroid theAsteroid = new Asteroid();
            SpaceShip theSpaceShip = new SpaceShip();
            ApolloSpacecraft theApolloSpacecraft = new ApolloSpacecraft();

            theAsteroid.CollideWith(theSpaceShip);
            theAsteroid.CollideWith(theApolloSpacecraft);

            ExplodingAsteroid theExplodingAsteroid = new ExplodingAsteroid();

            theExplodingAsteroid.CollideWith(theSpaceShip);
            theExplodingAsteroid.CollideWith(theApolloSpacecraft);

            IRock rock = new ExplodingAsteroid();

            rock.CollideWith(theSpaceShip);
            rock.CollideWith(theApolloSpacecraft);


            Asteroid theAsteroidReference = theExplodingAsteroid;
            theAsteroidReference.CollideWith(theSpaceShip);
            theAsteroidReference.CollideWith(theApolloSpacecraft);

            SpaceShip theSpaceShipReference = theApolloSpacecraft;
            theAsteroid.CollideWith(theSpaceShipReference);
            theAsteroidReference.CollideWith(theSpaceShipReference);

            theAsteroid.CollideWith(theSpaceShipReference as dynamic);
            theAsteroidReference.CollideWith(theSpaceShipReference as dynamic);

            Console.WriteLine("*********************End*********************");
        }
        interface IRock
        {
             void CollideWith(SpaceShip s);
             void CollideWith(ApolloSpacecraft s);
        };
        class SpaceShip {} 
        class ApolloSpacecraft : SpaceShip {}

        class Asteroid: IRock
        {
            public void CollideWith(SpaceShip s)
            {
                Console.WriteLine("Asteroid colid with SpaceShip.");
            }
            public void CollideWith(ApolloSpacecraft s)
            {
                Console.WriteLine("Asteroid colid with ApolloSpacecraft.");
            }
        }

        class ExplodingAsteroid:  Asteroid
        {
            new public void CollideWith(SpaceShip s)
            {
                Console.WriteLine("ExplodingAsteroid colid with SpaceShip.");
            }
            new public void CollideWith(ApolloSpacecraft s)
            {
                Console.WriteLine("ExplodingAsteroid colid with ApolloSpacecraft.");
            }
        }
        class Animal { }
        class Cat : Animal { }
        class Dog : Animal { }        
        class Mouse : Animal { }      
        void ReactSpecialization(Animal me, Animal other)
        {
            Console.WriteLine("{0} is not interested in {1}.", me, other);
        }
        void ReactSpecialization(Cat me, Dog other)
        {
            Console.WriteLine("Cat runs away from dog.");
        }
        void ReactSpecialization(Cat me, Mouse other)
        {
            Console.WriteLine("Cat chases mouse.");
        }
        void ReactSpecialization(Dog me, Cat other)
        {
            Console.WriteLine("Dog chases cat.");
        }
        void React(Animal me, Animal other)
        {
            ReactSpecialization(me as dynamic, other as dynamic);
        }

        
    }
}
