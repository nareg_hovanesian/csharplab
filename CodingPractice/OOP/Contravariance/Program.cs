﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpLab.OOP.Contravariance
{
    // https://www.youtube.com/watch?v=QHuN5dn2f0w
    public class Program
    {
        delegate void BaseDelegate(Base b);
        delegate void DriveDelegate(Drive b);
        public static void TestBase(Base b) { }
        public static void TestDrive(Drive b) { }
        public class Base
        {
            public static Base Factory() { return new Base(); }
        }
        public class Drive : Base
        {
            public static new Base Factory() { return new Drive(); }
        }

        public class BasePrinter
        {
            public void Printer(Drive b) { Console.WriteLine("BasePrinter Prints"); }
            public void Printer(IEnumerable<Drive> b) { Console.WriteLine("BasePrinter Prints"); }
        }
        public class DrivePrinter : BasePrinter
        {
            // overloading 
            public void Printer(Base b) { Console.WriteLine("DrivePrinter Prints"); }
           // public new void Printer(Drive b) { Console.WriteLine("DrivePrinter Prints 2"); }
            public void Printer(IEnumerable<Base> b) { Console.WriteLine("DrivePrinter Prints"); }

            //public void BasePrinter(Base [] b) { Console.WriteLine("DrivePrinter Prints"); }

            //public void DirvePrinter(Drive[] b) { Console.WriteLine("DrivePrinter Prints"); }            
        }

        public static void Test(params Base[] d) { }

        public static void FillArray(Base[] list, Func<Base> factor)
        {
            for (int i = 0; i < list.Length; i++)
            {
                list[i] = factor();
            }
        }
       
        public static void Main(string[] args)
        {
            Console.WriteLine("********************Start********************");

            // case 1:
            BaseDelegate t1 = TestBase;
            //BaseDelegate t2 = TestDrive;  // won't work
            DriveDelegate t3 = TestBase;
            DriveDelegate t4 = TestDrive;   // works because of Contravariance // https://www.youtube.com/watch?v=QHuN5dn2f0w

            // case 2:
            DrivePrinter d = new DrivePrinter();
            d.Printer(new Base());      //  ?
            d.Printer(new Drive());     //  ?

            BasePrinter b = new DrivePrinter();
            b.Printer(new Drive());     //  ?

            var obj = new DrivePrinter();
            obj.Printer(new Drive());   //  ?

            obj.Printer(new List<Drive> { new Drive(), new Drive() }); //  ? How about older versions - prior to C# 4.0 ?        
            IEnumerable<Base> lb = new List<Drive>();
            Base[] list = new Drive[5];                             // works!
            //List<Base> list = new List<Drive>();                  // won't work !

            if(list is Base[])
                Console.WriteLine(list.GetType());                  // ?
            
            // obj.BasePrinter(new Drive[] { new Drive(), new Drive() });
            // obj.DirvePrinter(new Drive[] { new Drive(), new Drive() });            


            FillArray(new Base[5], Base.Factory);                // works
            FillArray(new Base[5], Drive.Factory);               // works
            FillArray(new Drive[5], Drive.Factory);              // works !
            FillArray(new Drive[5], () => Drive.Factory());      // works 

            // Will cause run time error, throwing ArrayTypeMissmatch exeption
            // Because array's don't support contravariants             
            // FillArray(new Drive[5], Base.Factory);
            // FillArray(new Drive[5], () => Base.Factory);           

            UInt16 a = 0xffff;
            Byte[] bytes = BitConverter.GetBytes(a);
            Int32 outdate = BitConverter.ToInt16(bytes, 0); 

            Console.WriteLine(outdate);
            Console.WriteLine("a");
            Console.WriteLine("*********************End*********************");
        }



    }
}
