﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CSharpLab.Parallelism
{
    static class ExtendedMethods
    {
        // Effective C# book: Item 32
        public static Task<T>[] OrderByCompletiona<T>(this IEnumerable<Task<T>> tasks)
        {            
            var sourceTasks = tasks.ToList();

            var completionSources = new TaskCompletionSource<T>[sourceTasks.Count];

            var outputTasks = new Task<T>[sourceTasks.Count];

            for (int i = 0; i < completionSources.Length; i++)
            {
                completionSources[i] = new TaskCompletionSource<T>();
                outputTasks[i] = completionSources[i].Task;
            }

            // 1
            int nextTaskIndex = -1;
            Action<Task<T>> continuation = completed =>
            {
                var bucket = completionSources[Interlocked.Increment(ref nextTaskIndex)];
                if (completed.IsCompleted)                
                    bucket.TrySetResult(completed.Result);                
                else if (completed.IsFaulted)
                    bucket.TrySetException(completed.Exception);
            };

            // 2
            foreach (var inputTask in sourceTasks)
            {
                inputTask.ContinueWith(continuation, CancellationToken.None,
                    TaskContinuationOptions.ExecuteSynchronously, TaskScheduler.Default);
            }
            return outputTasks;

        }
    }
}
