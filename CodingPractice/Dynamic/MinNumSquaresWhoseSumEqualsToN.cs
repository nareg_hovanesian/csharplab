﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpLab.Dynamic
{
    class MinNumSquaresWhoseSumEqualsToN
    {
        public int AlgRecursive(int N)
        {
            if (N < 3)
                return N;      
            int res = N;
            for (int i = 1; i < N; i++)
            {
                int temp = i * i;
                if (temp > N)                
                    break;                
                res = Math.Min(1 + AlgRecursive(N - temp), res);
            }                      
            return res;
        }     
        public static void Main(string[] args)
        {
            Console.WriteLine("********************Start********************");

            var v = new MinNumSquaresWhoseSumEqualsToN();

            for (int i = 0; i < 35; i++)
            {
                int res = v.AlgRecursive(i);
                Console.Write(res.ToString() + ", ");
            }
            Console.WriteLine("");
            Console.WriteLine("*********************End*********************");
        }
    }
}
