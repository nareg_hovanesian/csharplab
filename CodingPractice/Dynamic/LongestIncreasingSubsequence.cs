﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpLab.Dynamic
{
    // http://www.geeksforgeeks.org/longest-increasing-subsequence/
    // http://www.geeksforgeeks.org/longest-monotonically-increasing-subsequence-size-n-log-n/
    public class LongestIncreasingSubsequence
    {        
        public int GetLongestIncreasingSubsequence(int[] set, int k)
        {
            int res = 0;
            if (set.Length > 0)
                res = 1;
            // 10, 4, 6
            // 4, 1, 10                 
            return res; 
        }
        // Recursive Approach 
        //public int LongestIncreasingSubsequence(int[] Coins, int Money)
        //{
        //    if (Money == 0)
        //        return 0;
        //    int min = int.MaxValue;
        //    foreach (var c in Coins)
        //    {
        //        if (Money < c)
        //            continue;
        //        int localMin = LongestIncreasingSubsequence(Coins, Money - c) + 1;
        //        if (min > localMin)
        //            min = localMin;
        //    }
        //    return min;
        //}
        public static void Main(string[] args)
        {
            Console.WriteLine("********************Start********************");

            var alg = new LongestIncreasingSubsequence();
            int[] set = { 3, 10, 4, 6, 20};
            Console.WriteLine("Coins: " + string.Join(", ", set));            
            int res = alg.GetLongestIncreasingSubsequence(set, 0);
            Console.WriteLine(res);
            Console.WriteLine("*********************End*********************");
        }
    }
}
