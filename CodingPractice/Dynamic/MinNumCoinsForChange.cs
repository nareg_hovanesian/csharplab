﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpLab.Dynamic
{
    // http://www.geeksforgeeks.org/find-minimum-number-of-coins-that-make-a-change/
    public class MinNumCoinsForChange
    {        
        public int MinCoinsDynamic(int[] Coins, int Money)
        {
            if (Money == 0)
                return 0;            
            Array.Sort(Coins);
            int[][] Matrix = new int[Coins.Length + 1][];
            for (int i = 0; i < Matrix.Length; i++)
            {
                Matrix[i] = new int[Money + 1];                
            }
            for (int i = 0; i < Matrix[0].Length; i++)
            {
                Matrix[0][i] = int.MaxValue;
            }
            for (int i = 1; i < Matrix.Length; i++)
            {
                for (int j = 1; j < Matrix[i].Length; j++)
                {
                    if(Coins[i - 1] > j)
                    {
                        Matrix[i][j] = Matrix[i - 1][j];
                    } else
                    {                                                
                        Matrix[i][j] = Math.Min(Matrix[i][j - Coins[i - 1]] + 1, Matrix[i - 1][j]);
                    }                    
                }
            }
            return Matrix[Matrix.Length-1][Matrix[0].Length-1];
        }
        // Recursive Approach 
        public int MinCoinsRecursive(int[] Coins, int Money)
        {
            if (Money == 0)
                return 0;
            int min = int.MaxValue;
            foreach (var c in Coins)
            {
                if (Money < c)
                    continue;
                int localMin = MinCoinsRecursive(Coins, Money - c) + 1;                
                min = (min > localMin) ? localMin : min;
            }
            return min;
        }
        public static void Main(string[] args)
        {
            Console.WriteLine("********************Start********************");

            var minNumCoinsForChange = new MinNumCoinsForChange();
            int[] Coins = { 1,5,7};
            Console.WriteLine("Coins: " + string.Join(", ", Coins.OrderByDescending(c => c)));            
            int res = minNumCoinsForChange.MinCoinsDynamic(Coins.OrderByDescending(c => c).ToArray(), 52);

            Console.WriteLine(res);
            Console.WriteLine("*********************End*********************");
        }        
     }
}
