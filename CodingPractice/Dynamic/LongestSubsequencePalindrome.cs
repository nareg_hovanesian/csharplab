﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpLab.Dynamic
{
    public class LongestSubsequencePalindrome
    {

        // 1 2 3 1
        //   1 2 
        // 5, 6, 1, 10, 12, 8, 10, 13, 7, 3
        // BAD ALGORITHM  
        public static int longestSubsequencePalindrome(List<int> arr, int s, int e)
        {
            if (arr.Count == 0)
                return 0;
            if (s == e)
                return 1;
            if (s > e || s < 0)
                return 0;
            for (int i = e; i > s; i--)
            {
                if (arr[s] == arr[i])
                    return 2 + longestSubsequencePalindrome(arr, s + 1, i - 1);
            }
            return 1;
        }

        // http://www.geeksforgeeks.org/dynamic-programming-set-12-longest-palindromic-subsequence/
        public static int longestSubsequencePalindromeOptimized(int[] arr, int s, int e)
        {
            if (s == e)
                return 1;
            if (arr[s] == arr[e] && s + 1 == e)
                return 2;
            if (arr[s] == arr[e])
                return 2 + longestSubsequencePalindromeOptimized(arr, s + 1, e - 1);

            return Math.Max(longestSubsequencePalindromeOptimized(arr, s + 1, e),
                            longestSubsequencePalindromeOptimized(arr, s, e - 1));
        }

        public static int longestSubsequencePalindromeDynamicOptimized(int[] arr)
        {
            int[][] matrix = new int[arr.Length][];
            for (int i = 0; i < arr.Length; i++)
                matrix[i] = new int[arr.Length];

            for (int i = 0; i < arr.Length; i++)
                matrix[i][i] = 1;

            for (int cl = 2; cl <= arr.Length; cl++)
            {
                for (int i = 0; i < arr.Length - cl + 1; i++)
                {
                    int j = i + cl - 1;
                    if (arr[i] == arr[j] && cl == 2)
                        matrix[i][j] = 2;
                    else if (arr[i] == arr[j])
                        matrix[i][j] = 2 + matrix[i + 1][j - 1];
                    else
                        matrix[i][j] = Math.Max(matrix[i + 1][j], matrix[i][j - 1]);
                }
            }
            return matrix[0][arr.Length - 1];
        }

        static void Main(string[] args)
        {
            Console.WriteLine("********************Start********************");

            Console.WriteLine("CountHandShakes: " );

            Console.WriteLine("*********************End*********************");
        }
    }
}
