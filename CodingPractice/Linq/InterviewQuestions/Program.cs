﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpLab.Linq.InterviewQuestions
{

    public abstract class Group<T> 
    {
        List<T> shapes { get; set; }
    }
         
    public class Shape
    {
        public string Name { get; set; }
    }
    public class Program
    {
        //  Filtering operations: Where, Take, Skip, etc.
        //  Project operations: Select, Join, etc.
        //  Ordering operations: OrderBy, ThenBy, Reverse.etc.
        //  Grouping operations: GroupBy
        //  Set operations: Concat, Union, etc.
        //  Element operations: First, FirstOrDefault, Last, etc.
        //  Aggregation operations: Count, Min, Max, Sum, etc.
        //  Qualifier operations: Contains, Any, All, etc.
        //  Conversion operations: ToArray, ToList, Cast, etc.
        class Product
        {
            public int Name { get; set; }
            public string Category { get; set; }
            public int Count { get; set; }            
        }

        // http://www.programminginterviews.info/2012/07/linq-interview-questions-part-3.html
        // https://blogs.msdn.microsoft.com/ericlippert/2009/05/18/foreach-vs-foreach/        
        //  The purpose of an expression is to compute a value, not to cause a side effect. 
        // The purpose of a statement is to cause a side effect. ~Eric Lippert        
        public static void Main(string[] args)
        {            
            Console.WriteLine("********************Start********************");                   
            // Question 0: Using LINQ, print out words which contain “ei”.
            {
                string search = "ei";
                var list = new List<string>() { "ei", "aei", "ab", "aeib"};
                // answer
                var answers = (from l in list
                              where l.Contains(search)                              
                              select l);                                                    
                Console.WriteLine(answers.Aggregate((st1, st2) => st1 + ", " + st2 ));
                
                // Question 1: Using LINQ, determine if any word in a list contains the substring “ei”.
                Console.WriteLine("Contains 'ei' {0}.", list.Any(s => s.Contains(search)));
            }
            // Question 2: Using LINQ, return a grouped list of products only for 
            // categories that have at least one product that is out of stock.
            {
                var products = new List<Product>();
                var answer = from p in products
                             group p by p.Category into Group
                             where Group.Any(p => p.Count == 0)
                             select new { Category = Group.Key, Products = Group };
            }
            {                
                // Question 3: Determine if an array of numbers contains all odds.
                int[] numbers = { 1, 11, 3, 19, 41, 65, 19 };                
                Console.WriteLine("Odd: {0}.", numbers.All(n => n % 2 != 0));
                Console.WriteLine("-----------------------------------------");
            }
            {
                //Question 4: Using LINQ, return a grouped list of products only for 
                // categories that have all of their products in stock.
                var products = new List<Product>();
                var answer = from p in products
                             group p by p.Category into Group
                             where Group.All(p => p.Count != 0)
                             select new { Category = Group.Key, Products = Group };
            }
            {
                // http://www.programminginterviews.info/2011/06/linq-skip-and-take-interview-questions.html
                // Question: Find all the fruits in a sequence as long as
                // their name length is more than the position they occupy in the sequence.
                // Note: it will discountinue looking further   as soon as found expression returns false.
                string[] fruits = { "apple", "passionfruit", "banana", "mango", "orange", "blueberry", "grape", "strawberry" };
                IEnumerable<string> query = fruits.TakeWhile((fruit, index) => fruit.Length >= index);
                foreach (string fruit in query)
                {
                    Console.WriteLine(fruit);
                }
                Console.WriteLine("-----------------------------------------");
            }
            {
                // http://www.programminginterviews.info/2011/06/linq-group-sort-and-count-words-in.html
                //Question: Given a sentence, group the words of same length, sort the groups
                // in increasing order and display the groups, the word count and the words.
                string input = "LINQ can be used to group words and count them too";
                
                var answer = from str in input.Split(' ')
                          group str by str.Length into g
                          orderby g.Key 
                          select new { Length = g.Key, Words = g, Count = g.Count() };
                
                foreach (var strs in answer)
                {
                    Console.WriteLine("Words Length: {0}, Count: {1}",  strs.Length, strs.Count );
                    foreach (var str in strs.Words)
                    {
                        Console.WriteLine(str);
                    }
                }
                Console.WriteLine("-----------------------------------------");
            }
            {
                // https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/keywords/let-clause
                string[] strings = {
                                    "A penny saved is a penny earned.",
                                    "The early bird catches the worm.",
                                    "The pen is mightier than the sword."
                                    };

                // Split the sentence into an array of words
                // and select those whose first letter is a vowel.
                var earlyBirdQuery =
                    from sentence in strings
                    let words = sentence.Split(' ')
                    from word in words
                    let w = word.ToLower()
                    where w[0] == 'a' || w[0] == 'e'
                        || w[0] == 'i' || w[0] == 'o'
                        || w[0] == 'u'
                    select word;

                // Execute the query.
                foreach (var v in earlyBirdQuery)
                {
                    Console.WriteLine("\"{0}\" starts with a vowel", v);
                }
                Console.WriteLine("-----------------------------------------");
            }           
                        
            {
                var Shapes = new List<Shape>
                {
                    new Shape() { Name = "Rectangle" },
                    new Shape() { Name = "Circle" }
                };
                var t1 = Shapes.Select(s => s).ToList();
                var t2 = Shapes;
                var sh = Shapes[1];
                sh.Name = "Test";

                Shapes.RemoveAt(0);
                Console.WriteLine(string.Join(", ", Shapes.Select(s => s.Name)));
                Console.WriteLine(string.Join(", ", t1.Select(s => s.Name)));
                Console.WriteLine(string.Join(", ", t2.Select(s => s.Name)));
                Console.WriteLine("-----------------------------------------");
            }
            {
                var list = Enumerable.Range(1, 5).ToList();
                list = list.Where((i, a) => a > 3).ToList();
                list.ForEach(Console.WriteLine);
                Console.WriteLine("-----------------------------------------");
            }
            {
                dynamic d = new System.Dynamic.ExpandoObject();
                                
                d.city = "Test";
                d.city = 1;
                var g = new { city = "test" };
                Console.WriteLine(d.city);
                Console.WriteLine(g.city);

                Console.WriteLine("-----------------------------------------");
            }
            {

                Console.WriteLine("Test");
            }
            Console.WriteLine("*********************End*********************");
        }      
    }
   
}
